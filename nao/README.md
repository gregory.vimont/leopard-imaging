# Architecture
NAO's head contains two cameras
* One 2D USB 3.0 Cameras using a [CX3](https://www.e-consystems.com/CX3-Reference-Design-Kit.asp) and a [OV5640](http://www.ovt.com/products/sensor.php?id=177) (top camera).
* One 2D USB 2.0 Cameras using a [CX3](https://www.e-consystems.com/CX3-Reference-Design-Kit.asp) and a [OV5640](http://www.ovt.com/products/sensor.php?id=177) (bottom camera).

## 2D Cameras
A udev rule located at /etc/udev/rules.d/42-usb-cx3.rules is
responsible to upload the firmware when the bootloader of the CX3 is detected.

It also creates the symlinks `video-top` and `video-bottom`

# Find Robot IP
Usually when you plug an ethernet cable on the head, Robot should automatically get
an IP address on your local network (connman manager use dhcpcd daemon like).

To retrieve this IP, the faster way is to use the mini usb port (like any 
android smartphone) which is in fact, a serial port (aka ttyUSB).

First install **minicom**
```sh
sudo apt-get update
sudo apt-get install minicom
```
You can use the following function to setup and launch minicom (i.e. to put in your .zshrc for example)
```sh
# Need minicom to use serial port on robot
ttyusb() {
  sudo ln -sf /dev/ttyUSB[0-9] /dev/ttyUSBX
  echo `ls -la /dev/ttyUSBX`
  sudo sh -c 'echo -e "pu port /dev/ttyUSBX\npu rtscts No" > /etc/minirc.dfl'
  sudo minicom --color=on
}
```

After you just need to tip:
```sh
ttyusb
```
Then enter your password (sudo need root access)
and you should be able to see a terminal on robot.

login: nao  
password: nao

Finally to retrieve the ip address just write, in the minicom terminal, the command:
```sh
ip a
```

# SSH to Robot
In an terminal simply use:
```sh
ssh nao@[NAO_IP]
```

# Streaming Video
To stream video from robot to your desktop, the simplest solution is to use
gstreamer on robot and vlc or gstreamer on your computer as follow...  
On Robot:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video-top ! video/x-raw-yuv,width=640,height=480,framerate=30/1 ! ffmpegcolorspace ! jpegenc ! multipartmux! tcpserversink port=3000 
gst-launch-0.10 -v v4l2src device=/dev/video-bottom ! video/x-raw-yuv,width=640,height=480,framerate=30/1 ! ffmpegcolorspace ! jpegenc ! multipartmux! tcpserversink port=3001 
```

On Desktop:
Top Camera is mounted upside down so the picture will be upside down...

The video stream can be played with VLC
```sh
vlc tcp://[NAO_IP]:3000
vlc tcp://[NAO_IP]:3001
```

# Update CX3 firmware
Let's assume that the new firmware file is `new_firmware.img`

Copy the new firmware from the PC to the robot :
On the PC
```sh
scp new_firmware.img nao@[NAO_IP]:
```

Get r/w access on the robot and update the link to the firmware:
* For Bottom camera (USB2)
```sh
su
mount -o remount,rw /
rm /usr/share/firmware/CX3RDK_OV5640_USB2.img
ln -s /home/nao/new_firmware.img /usr/share/firmware/CX3RDK_OV5640_USB2.img
exit
```

* For Top camera (USB3)
```sh
su
mount -o remount,rw /
rm /usr/share/firmware/CX3RDK_OV5640_USB3.img
ln -s /home/nao/new_firmware.img /usr/share/firmware/CX3RDK_OV5640_USB3.img
exit
```

* Load the new firmware
The following command will toggle the reset line of both CX3.
The new fw will be automatically loaded.
```sh
/usr/libexec/reset-cameras.sh toggle
```


