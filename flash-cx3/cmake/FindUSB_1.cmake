# - Try to find libusb-1.0
# Once done this will define
#  USB_1_FOUND - System has libusb-1.0
#  USB_1_INCLUDE_DIRS - The libusb-1 include directories
#  USB_1_LIBRARIES - The libraries needed to use libusb-1.0

find_path(USB_1_INCLUDE_DIR libusb-1.0/libusb.h
  PATHS /usr/incude/libusb-1.0
  PATH_SUFFIXES libusb-1.0 )

find_library(USB_1_LIBRARY NAMES libusb-1.0.so
  PATHS /usr/lib64 /usr/lib/x86_64-linux-gnu)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set USB_1_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(USB_1  DEFAULT_MSG USB_1_INCLUDE_DIR USB_1_LIBRARY)

mark_as_advanced(USB_1_INCLUDE_DIR USB_1_LIBRARY)

set(USB_1_INCLUDE_DIRS ${USB_1_INCLUDE_DIR} )
set(USB_1_LIBRARIES ${USB_1_LIBRARY} )
