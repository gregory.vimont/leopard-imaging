# Description
You can find here, a program to flash a firmware for the camera LI-OV5640 aka OV5640 -> CX3 -> USB 3.0.  

# HowTo Build
On your host you can build using pure CMake build, for robot, you'll need qiBuild and do a cross compilation toolchain...  
note: Read doc in Toolchain directory for building using qiBuild.

## Dependencies
Project use CMake >= 3.2, C++11 (i.e. program is linux only).  

## Pure CMake build (Native/Host build)
On Desktop (i.e. native build):
```sh
mkdir build && cd build
cmake ..
make
```

## Desktop qiBuild build (Native/Host build)
First you need to configure and build using qiBuild tools:
```sh
qibuild configure --release
qibuild make
```

# HowTo Test
If you want program usage just call it without argument, like usual:
```sh
$ ./build/bin/flash-cx3 -h
usage: 
./build/bin/flash-cx3 [OPTIONS] -p bus:port.port -f file
./build/bin/flash-cx3 [OPTIONS] -d vendor:product -f file
-p, --path <bus:port.port>: Specify device by its path (e.g. -p 3:1.2, for bus 3 and ports 1.2)
-d, --device <vendor:product>: Specify device by its vendorID:ProductID (e.g. -d 04b4:00f3)
-f <file>: Flash the device with the firmware <file>
OPTIONS:
-h, --help: Show usage and help
-v, --verbose: enable verbose log
```

## On Desktop
First you need to determine which bus/port(s) device correspond to your camera
```sh
lsusb
```
Output example
```sh
[...]
Bus 003 Device 003: ID 04b4:00f3 Cypress Semiconductor Corp.
```
So here camera use **BUS 3** and **device 3**  
Know you can retrieve ports
```sh
lsusb -t
```
Output example
```sh
[...]
/:  Bus 03.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/4p, 480M
 |__ Port 5: Dev 2, If 0, Class=Hub, Driver=hub/6p, 480M
    |__ Port 2: Dev 3, If 0, Class=Vendor Specific Class, Driver=, 480M
```
So here Camera use *Bus3* and *Port5.Port2*.  
note: Linux and libusb ignore the *Port* associated to the bus.

Then, once the project has been build, you can test it using:  
```sh
./build/bin/flash-cx3 --path 3:5.2 -f firmware/CX3RDK_OV5640_XXX.img
```
You can also use the vendorID:productID
```sh
./build/bin/flash-cx3 --device 04b4:00f3 -f firmware/CX3RDK_OV5640_XXX.img
```
warning: On robot both devices have the same ID !

## On Robot
On robot top and bottom camera will **always** be /dev/video-top and
/dev/video-bottom respectively.  
After having deployed program on robot.  
You can run it, using:
```sh
ssh nao@ip.of.the.robot
./<target_destination_dir>/bin/flash-cx3 -p BUS:PORT.PORT -f CX3RDK_OV5640_XXX.img
```
