/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#pragma once

#include <libusb-1.0/libusb.h>
#include <ostream>
#include <vector>

class FX3Device {
	public:
	//! @brief This function is only useful if you know in advance how the hardware is
	//! connected.
	//! @param[in] bus Bus number.
	//! @param[in] ports Ports list.
	FX3Device(std::uint8_t bus,
	          const std::vector<std::uint8_t>& ports,
	          bool verbose = false);

	//! @brief This function is only useful if you know in advance that there is only
	//! one device with the given VID and PID attached to the host system.
	//! @param[in] vendorID Vendor ID.
	//! @param[in] productID Product ID.
	FX3Device(std::uint16_t vendorID, std::uint16_t productID, bool verbose = false);

	~FX3Device();

	//! @brief Open and claim interface of the device.
	void open();
	//! @brief Release interface and close the device.
	//! @details do nothing if device already close.
	void close();
	//! @brief Check if device is open and ready to flash.
	//! @return True if device owned, false otherwise.
	bool isOpen() const;

	//! @brief Flash firmware.
	//! @throw Device is open.
	//! @param[in] filename File path of the firmware.
	void flash(const std::string& filename);

	//! @brief Get the Vendor ID of the device.
	//! @pre _device is non-null.
	std::uint16_t getVendorID() const;
	//! @brief Get the Product ID of the device.
	//! @pre _device is non-null.
	std::uint16_t getProductID() const;

	//! @brief Get Bus number pertaining to the device.
	//! @pre _device is non-null.
	std::uint8_t getBus() const;
	//! @brief Get Port(s) number(s) pertaining to the device.
	//! @pre _device is non-null.
	std::vector<std::uint8_t> getPorts() const;

	//! @brief Verify is device is a potential CX3/FX3.
	static bool isDeviceOfInterest(libusb_device* _device);

	private:
	bool _verbose;
	libusb_context* _context;
	libusb_device* _device;
	libusb_device_handle* _handle;
	friend std::ostream& operator<<(std::ostream& stream, const FX3Device& device);
};
