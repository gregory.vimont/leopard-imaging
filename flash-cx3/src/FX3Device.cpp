/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#include "FX3Device.hpp"
#include <array>
#include <cstring> // std::strerror
#include <fstream>
#include <iomanip> // std::setw, std::setfill
#include <iostream>

std::ostream&
operator<<(std::ostream& stream, const FX3Device& device) {
	stream << std::hex << std::setw(4) << std::setfill('0') << device.getVendorID() << ":"
	       << std::setw(4) << std::setfill('0') << device.getProductID();
	stream << std::dec << " (Bus:" << int(device.getBus()) << ", Port:";
	std::vector<std::uint8_t> ports = device.getPorts();
	for (std::size_t i = 0; i < ports.size(); ++i) {
		if (i != 0) stream << ".";
		stream << int(ports[i]);
	}
	stream << ")";
	return stream;
}

std::vector<std::uint8_t>
_getPorts(libusb_device* dev) {
	std::vector<std::uint8_t> ports;
	{
		std::array<std::uint8_t, 32> tmp;
		int count = libusb_get_port_numbers(dev, tmp.begin(), 32);
		for (int i = 0; i < count; ++i) {
			ports.push_back(tmp[i]);
		}
	}
	return ports;
}

FX3Device::FX3Device(std::uint8_t bus,
                     const std::vector<std::uint8_t>& ports,
                     bool verbose)
  : _verbose(verbose)
  , _context(nullptr)
  , _device(nullptr)
  , _handle(nullptr) {
	std::cout << "(INFO) Search Device with "
	          << "bus: " << int(bus) << ", port(s): ";
	for (std::size_t i = 0; i < ports.size(); ++i) {
		if (i != 0) std::cout << ".";
		std::cout << int(ports[i]);
	}
	std::cout << std::endl;

	if (libusb_init(&_context)) {
		throw std::runtime_error("Can't initialize libusb library");
	}

	libusb_device** list;
	ssize_t numdev = libusb_get_device_list(NULL, &list);
	if (numdev < 0) {
		throw std::runtime_error("Library: Error in enumerating devices...");
	}
	for (ssize_t i = 0; i < numdev; ++i) {
		libusb_device* dev = list[i];
		if (libusb_get_bus_number(dev) == bus && _getPorts(dev) == ports &&
		    isDeviceOfInterest(dev)) {
			_device = dev;
			break;
		}
	}
	libusb_free_device_list(list, 1);
	if (!_device) {
		throw std::runtime_error("Device with specified path not found");
	}
}

FX3Device::FX3Device(std::uint16_t vendorID, std::uint16_t productID, bool verbose)
  : _verbose(verbose)
  , _context(nullptr)
  , _device(nullptr)
  , _handle(nullptr) {
	std::cout << "(INFO) Search device " << std::hex << std::setw(4) << std::setfill('0')
	          << vendorID << ":" << std::setw(4) << std::setfill('0') << productID
	          << std::dec << std::endl;

	if (libusb_init(&_context)) {
		throw std::runtime_error("Can't initialize libusb library");
	}

	libusb_device_handle* handle =
	  libusb_open_device_with_vid_pid(_context, vendorID, productID);
	if (!handle)
		throw std::runtime_error("Device with specified vendorID:productID not found");
	_device = libusb_get_device(handle);
	libusb_close(handle);
}

FX3Device::~FX3Device() {
	close();
	_device = nullptr;
	libusb_exit(_context);
	_context = nullptr;
}

void
FX3Device::open() {
	if (isOpen()) return;
	if (_verbose) std::cout << "(VERBOSE) Open device..." << std::endl;
	if (libusb_open(_device, &_handle)) {
		throw std::runtime_error(std::string("Can't open device: ") + std::strerror(errno));
	}

	if (_verbose) std::cout << "(VERBOSE) Claim device..." << std::endl;
	if (libusb_claim_interface(_handle, 0)) {
		throw std::runtime_error(std::string("Can't claim device: ") +
		                         std::strerror(errno));
	}
}

void
FX3Device::close() {
	if (isOpen()) {
		if (_verbose) std::cout << "(VERBOSE) Release device..." << std::endl;
		if (0 != libusb_release_interface(_handle, 0)) {
			libusb_close(_handle);
			_handle = nullptr;
			throw std::runtime_error(std::string("Can't release device: ") +
			                         std::strerror(errno));
		}
		if (_verbose) std::cout << "(VERBOSE) Close device..." << std::endl;
		libusb_close(_handle);
		_handle = nullptr;
	}
}

bool
FX3Device::isOpen() const {
	return _handle != nullptr;
}

void
FX3Device::flash(const std::string& filename) {
	std::ifstream fwFile(filename, std::ifstream::binary);
	if (!fwFile.is_open()) {
		throw std::runtime_error("Firmware file not found !");
	}
	open();
	std::cout << "(INFO) Flash firmware..." << std::endl;
	std::uint32_t length   = 0;
	std::uint32_t addr     = 0;
	std::uint32_t checksum = 0;
	// ignore first 4 byte (Magic)
	fwFile.read(reinterpret_cast<char*>(&addr), 4);
	do { // Read each Block
		fwFile.read(reinterpret_cast<char*>(&length), 4);
		fwFile.read(reinterpret_cast<char*>(&addr), 4);
		if (_verbose && length > 0) {
			std::cerr << "(VERBOSE) Write Block at 0x" << std::hex << addr
			          << " (size: " << std::dec << 4 * length << "Byte)" << std::endl;
		}

		std::vector<std::uint32_t> fwBuffer(length, 0);
		fwFile.read(reinterpret_cast<char*>(&fwBuffer[0]), 4 * fwBuffer.size());
		for (std::uint32_t i = 0; i < length; i++) // Compute Checksum
			checksum += fwBuffer[i];
		{ // Split each block in 2048 Byte sub part.
			std::int32_t tmp_len   = 4 * length;
			std::uint32_t tmp_addr = addr;
			std::uint32_t index    = 0;
			while (tmp_len > 0) {
				std::int32_t size = std::min(2 * 1024, tmp_len);
				libusb_control_transfer(_handle,
				                        0x40,
				                        0xA0,
				                        tmp_addr & 0xffff,
				                        tmp_addr >> 16,
				                        reinterpret_cast<std::uint8_t*>(&fwBuffer[index / 4]),
				                        size,
				                        5000);
				tmp_len -= size;
				tmp_addr += size;
				index += size;
			}
		}
	} while (length != 0);
	std::uint32_t expectedChecksum;
	fwFile.read(reinterpret_cast<char*>(&expectedChecksum), 4);
	if (_verbose) {
		std::cerr << "(VERBOSE) Expected Checksum: " << expectedChecksum << std::endl;
		std::cerr << "(VERBOSE) Calculated Checksum: " << checksum << std::endl;
	}
	if (expectedChecksum != checksum) {
		std::cerr << "(ERROR) Checksum mismatch !" << std::endl;
		throw std::runtime_error("Checksum mismatch !");
	}
	std::cout << "(INFO) Flash firmware...DONE" << std::endl;
	// Reset the device
	libusb_control_transfer(
	  _handle, 0x40, 0xA0, addr & 0xffff, addr >> 16, NULL, 0, 5000);
	if (_verbose) std::cout << "(VERBOSE) Release device..." << std::endl;
	libusb_release_interface(_handle, 0);
	if (_verbose) std::cout << "(VERBOSE) Close device..." << std::endl;
	libusb_close(_handle);
	_handle = nullptr;
}

std::uint16_t
FX3Device::getVendorID() const {
	struct libusb_device_descriptor desc;
	libusb_get_device_descriptor(_device, &desc);
	return desc.idVendor;
}

std::uint16_t
FX3Device::getProductID() const {
	struct libusb_device_descriptor desc;
	libusb_get_device_descriptor(_device, &desc);
	return desc.idProduct;
}

std::uint8_t
FX3Device::getBus() const {
	return libusb_get_bus_number(_device);
}

std::vector<std::uint8_t>
FX3Device::getPorts() const {
	return _getPorts(_device);
}

bool
FX3Device::isDeviceOfInterest(libusb_device* device) {
	if (device == nullptr) throw std::invalid_argument("Device must be non-null");
	struct libusb_device_descriptor desc;
	libusb_get_device_descriptor(device, &desc);
	if ((0x04b4 == desc.idVendor) &&
	    ((0x00f3 == desc.idProduct) || (0x00c3 == desc.idProduct) ||
	     (0x0053 == desc.idProduct))) {
		return true;
	}
	return false;
}
