#!/usr/bin/env bash

set -x
mkdir ${HOME}/ctc
tar xzf ctc-linux64-atom-2.7.0.161.tar.gz -C ${HOME}/ctc/
qitoolchain create robot-ctc ${HOME}/ctc/ctc-linux64-atom-2.7.0.161/toolchain.xml
qibuild add-config -t robot-ctc robot
