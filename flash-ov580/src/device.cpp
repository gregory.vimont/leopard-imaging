/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#include "device.hpp"

#include <array>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <vector>

extern "C" {
#include <fcntl.h>  // ::open
#include <unistd.h> // ::close
}

#define OV580_RW_UNIT 0x02
#define OV580_COMMAND_SUB_TYPE_WRITE 0x02
#define OV580_COMMAND_SUB_TYPE_READ 0x03
#define OV580_COMMAND_SUB_TYPE_ERASE_SECTOR 0x20

#define OV580_COMMAND_ID 0x51
#define OV580_COMMAND_TYPE_FLASH 0xA1
#define OV580_FLASH_FLAGS_FIRST_PKG 1 << 7
#define OV580_FLASH_FLAGS_LAST_PKG 1 << 4

#define SPI_PAGE_SIZE 256
#define SPI_FLASH_SIZE 0x14000

Device::Device(const std::string& deviceName, bool verbose)
  : _verbose(verbose)
  , _device(-1) {
	_device = ::open(deviceName.c_str(), 0);
}

Device::~Device() {
	::close(_device);
	_device = -1;
}

void
Device::readFirmware(const std::string& fwFileName) {
	std::ofstream outFile(fwFileName, std::ofstream::binary);
	if (!outFile.is_open()) {
		std::cerr << "(ERROR) Can't open dump file: " << fwFileName << std::endl;
		throw std::runtime_error("Can't open dump file");
	}
	std::cout << "(INFO) Reading firmware..." << std::endl;
	for (std::uint32_t address = 0; address < SPI_FLASH_SIZE; address += SPI_PAGE_SIZE) {
		std::array<std::uint8_t, SPI_PAGE_SIZE> buffer;
		SPIReadPage(address, buffer.begin(), SPI_PAGE_SIZE);
		outFile.write(reinterpret_cast<char*>(buffer.begin()), SPI_PAGE_SIZE);
	}
	std::cout << "(INFO) Reading firmware...DONE" << std::endl;
}

bool
Device::checkFirmware(const std::string& fwFileName) {
	std::ifstream fwFile(fwFileName, std::ifstream::binary);
	if (!fwFile.is_open()) {
		std::cerr << "(ERROR) Can't load firmware file: " << fwFileName << std::endl;
		throw std::runtime_error("Can't load firmware file");
	}

	bool success = true;
	std::cout << "(INFO) Check firmware..." << std::endl;
	std::array<std::uint8_t, SPI_PAGE_SIZE> fwBuffer;
	std::uint32_t address = 0;
	while (
	  fwFile.read(reinterpret_cast<char*>(fwBuffer.begin()), SPI_PAGE_SIZE).gcount() >
	  0) {
		std::array<std::uint8_t, SPI_PAGE_SIZE> flashBuffer;
		SPIReadPage(address, flashBuffer.begin(), fwFile.gcount());
		int n = ::memcmp(fwBuffer.begin(), flashBuffer.begin(), fwFile.gcount());
		if (n != 0) {
			std::cerr << "(ERROR) data are differents at 0x" << std::hex << address
			          << " byte: " << std::dec << std::abs(n) << std::endl;
			success = false;
		}
		address += fwFile.gcount();
	}
	return success;
}

void
Device::writeFirmware(const std::string& fwFileName) {
	std::ifstream fwFile(fwFileName, std::ifstream::binary);
	if (!fwFile.is_open()) {
		std::cerr << "(ERROR) Can't load firmware file: " << fwFileName << std::endl;
		throw std::runtime_error("Can't load firmware file");
	}

	std::cout << "(INFO) SPI Weird Command." << std::endl;
	SPIWeird();
	std::cout << "(INFO) Erase SPI." << std::endl;
	SPIErase();
	std::cout << "(INFO) Writing firmware..." << std::endl;
	std::array<std::uint8_t, SPI_PAGE_SIZE> fwBuffer;
	std::uint32_t address = 0;
	while (
	  fwFile.read(reinterpret_cast<char*>(fwBuffer.begin()), SPI_PAGE_SIZE).gcount() >
	  0) {
		// Will try 5 time to write each buffer...
		bool success       = false;
		std::uint8_t count = 0;
		do {
			try {
				SPIWritePage(address, fwBuffer.begin(), fwFile.gcount());
				success = true;
			} catch (std::exception& e) {
				std::cerr << "(ERROR) Can't write firmware at address 0x" << std::hex << address
				          << std::dec << ": " << e.what() << std::endl;
			}
		} while (!success && count++ < 5);

		if (!success) {
			std::cerr << "(CRITICAL) Can't flash firmware !!!" << std::endl;
			throw std::runtime_error(std::strerror(errno));
		}
		address += fwFile.gcount();
	}
	std::cout << "(INFO) Writing firmware...DONE" << std::endl;
}

extern "C" {
#include <linux/usb/video.h> // define UVC_*
#include <linux/uvcvideo.h>  // UVC ExtUnit control
#include <sys/ioctl.h>       // ::ioctl
}

void
error_handle() {
	std::string err;
	switch (errno) {
		case ENOENT:
			err = "Extension unit or control not found";
			break;
		case ENOBUFS:
			err = "Buffer size does not match control size";
			break;
		case EINVAL:
			err = "Invalid request code";
			break;
		case EBADRQC:
			err = "Request not supported by control";
			break;
		default:
			err = std::string(std::strerror(errno));
			break;
	}
	std::cerr << "(ERROR) failed: " << err << " (System code: " << errno << ")"
	          << std::endl;
}

void
Device::getFirmwareVersion() {
	std::uint8_t value[384];
	std::memset(value, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = value;
	value[0]          = 0x51;
	value[1]          = 0x02;

	if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		error_handle();
		throw std::runtime_error(std::strerror(errno));
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(5));
	xu_query.query = UVC_GET_CUR;
	if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		error_handle();
		throw std::runtime_error(std::strerror(errno));
	}
	// DataSheet: Version Byte 16~19 but need to add the 1Byte shift.
	std::uint32_t version =
	  std::uint32_t(value[20] << 24 | value[19] << 16 | value[18] << 8 | value[17]);
	std::cout << "(INFO) Firmware version: " << std::hex << version << std::endl;
}

void
Device::SPIWeird() {
	// Step 1
	{
		std::cerr << "(INFO) Command Sub Type 0x39 Step 1" << std::endl;
		std::uint8_t value[384];
		std::memset(value, 0, 384);

		struct uvc_xu_control_query xu_query;
		std::memset(&xu_query, 0, sizeof(xu_query));
		xu_query.unit     = 4;
		xu_query.selector = OV580_RW_UNIT;
		xu_query.query    = UVC_SET_CUR;
		xu_query.size     = 384;
		xu_query.data     = value;

		value[0] = OV580_COMMAND_ID;
		value[1] = OV580_COMMAND_TYPE_FLASH;
		value[2] = 0x39;
		/* reserved */
		value[3] = 0x00;
		value[4] = 0x00;
		value[5] = 0x00;
		value[6] = 0x00;
		value[7] = 0x00;
		value[8] = 0x00;

		if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
			error_handle();
			throw std::runtime_error(std::strerror(errno));
		}

		xu_query.query = UVC_GET_CUR;
		do {
			std::this_thread::sleep_for(std::chrono::milliseconds(5));
			if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
				error_handle();
				throw std::runtime_error(std::strerror(errno));
			}
			if (value[0] == 0x56 || value[0] == 0x54) break;
		} while (true);
	}
	// Step 2
	{
		std::cerr << "(INFO) Command Sub Type 0x39 Step 2" << std::endl;
		std::uint8_t value[384];
		std::memset(value, 0, 384);

		struct uvc_xu_control_query xu_query;
		std::memset(&xu_query, 0, sizeof(xu_query));
		xu_query.unit     = 4;
		xu_query.selector = OV580_RW_UNIT;
		xu_query.query    = UVC_SET_CUR;
		xu_query.size     = 384;
		xu_query.data     = value;

		value[0] = OV580_COMMAND_ID;
		value[1] = OV580_COMMAND_TYPE_FLASH;
		value[2] = 0x39;
		/* reserved */
		value[3] = 0x00;
		value[4] = 0x00;
		value[5] = 0x00;
		value[6] = 0x00;
		value[7] = 0x00;
		value[8] = 0x1C;

		if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
			error_handle();
			throw std::runtime_error(std::strerror(errno));
		}

		xu_query.query = UVC_GET_CUR;
		do {
			std::this_thread::sleep_for(std::chrono::milliseconds(5));
			if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
				error_handle();
				throw std::runtime_error(std::strerror(errno));
			}
			if (value[0] == 0x56 || value[0] == 0x54) break;
		} while (true);
	}
}

void
Device::SPIReadPage(std::uint32_t isp_address,
                    std::uint8_t* buffer,
                    std::uint16_t length) {
	if (length > SPI_PAGE_SIZE) {
		std::cerr << "Buffer too large, max: " << SPI_PAGE_SIZE << ", got: " << length
		          << std::endl;
		throw std::length_error("Maximum allowed length: 256");
	}

	if (_verbose) {
		std::cout << "(VERBOSE) Read SPI address 0x" << std::hex << isp_address << std::dec
		          << " (" << isp_address << "), length: " << length << std::endl;
	}
	std::uint8_t value[384];
	std::memset(value, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = OV580_RW_UNIT;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = value;

	value[0] = OV580_COMMAND_ID;
	value[1] = OV580_COMMAND_TYPE_FLASH;
	value[2] = OV580_COMMAND_SUB_TYPE_READ;
	/* reserved */
	value[3] = 0x00;
	value[4] = 0x00;
	/* address */
	value[5] = (isp_address >> 24) & 0xff;
	value[6] = (isp_address >> 16) & 0xff;
	value[7] = (isp_address >> 8) & 0xff;
	value[8] = isp_address & 0xff;
	/* flags */
	value[9]  = OV580_FLASH_FLAGS_FIRST_PKG | OV580_FLASH_FLAGS_LAST_PKG | (length >> 8);
	value[10] = length & 0xFF;
	/* len */
	value[11] = (length >> 8) & 0xff;
	value[12] = length & 0xff;

	value[13] = 0x00;
	value[14] = 0x00;
	value[15] = 0x00;

	if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		error_handle();
		throw std::runtime_error(std::strerror(errno));
	}

	xu_query.query = UVC_GET_CUR;
	do {
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
		if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
			error_handle();
			throw std::runtime_error(std::strerror(errno));
		}
		if (value[0] == 0x56 || value[0] == 0x54) break;
	} while (true);

	// all value are shifted by one byte -> data start @17 nor @16.
	// value[0] seems to be used as a FLAG.
	::memcpy(buffer, value + 17, length);
}

void
Device::SPIWritePage(std::uint32_t isp_address,
                     std::uint8_t* buffer,
                     std::uint16_t length) {
	if (length > SPI_PAGE_SIZE) {
		fprintf(stderr, "Length is too large max 256 got: %d\n", length);
		throw std::range_error("Length parameter too large");
	}

	if (_verbose) {
		std::cout << "(VERBOSE) Write SPI address 0x" << std::hex << isp_address << std::dec
		          << " (" << isp_address << "), length: " << length << std::endl;
	}
	std::uint8_t value[384];
	std::memset(value, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = OV580_RW_UNIT;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = value;

	value[0] = OV580_COMMAND_ID;
	value[1] = OV580_COMMAND_TYPE_FLASH;
	value[2] = OV580_COMMAND_SUB_TYPE_WRITE;
	/* reserved */
	value[3] = 0x00;
	value[4] = 0x00;
	/* address */
	value[5] = (isp_address >> 24) & 0xff;
	value[6] = (isp_address >> 16) & 0xff;
	value[7] = (isp_address >> 8) & 0xff;
	value[8] = isp_address & 0xff;
	/* flags */
	value[9] =
	  OV580_FLASH_FLAGS_FIRST_PKG | OV580_FLASH_FLAGS_LAST_PKG | (length >> 1 * 8);
	value[10] = length & 0xFF;
	/* len */
	value[11] = (length >> 8) & 0xff;
	value[12] = length & 0xff;

	value[13] = 0x00;
	value[14] = 0x00;
	value[15] = 0x00;

	::memcpy(value + 16, buffer, length);

	if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		error_handle();
		throw std::runtime_error(std::strerror(errno));
	}

	xu_query.query = UVC_GET_CUR;
	do {
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
		if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
			error_handle();
			throw std::runtime_error(std::strerror(errno));
		}
		if (value[0] == 0x56 || value[0] == 0x54) break;
	} while (true);
}

void
Device::SPIErasePage(std::uint32_t isp_address) {
	if (_verbose) {
		std::cout << "(VERBOSE) Erase SPI address 0x" << std::hex << isp_address << std::dec
		          << " (" << isp_address << ")" << std::endl;
	}
	std::uint8_t value[384];
	std::memset(value, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = OV580_RW_UNIT;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = value;

	value[0] = OV580_COMMAND_ID;
	value[1] = OV580_COMMAND_TYPE_FLASH;
	value[2] = OV580_COMMAND_SUB_TYPE_ERASE_SECTOR;
	/* reserved */
	value[3] = 0x00;
	value[4] = 0x00;
	/* address */
	value[5] = (isp_address >> 24) & 0xff;
	value[6] = (isp_address >> 16) & 0xff;
	value[7] = (isp_address >> 8) & 0xff;
	value[8] = isp_address & 0xff;

	if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		error_handle();
		throw std::runtime_error(std::strerror(errno));
	}

	xu_query.query = UVC_GET_CUR;
	do {
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
		if (::ioctl(_device, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
			error_handle();
			throw std::runtime_error(std::strerror(errno));
		}
		if (value[0] == 0x56 || value[0] == 0x54) break;
	} while (true);
}

#define SPI_ERASE_SECTOR_SIZE 0x1000

void
Device::SPIErase() {
	for (std::uint32_t address = 0; address < SPI_FLASH_SIZE;
	     address += SPI_ERASE_SECTOR_SIZE) {
		SPIErasePage(address);
	}
}

std::ostream&
operator<<(std::ostream& out, const std::array<std::uint8_t, SPI_PAGE_SIZE>& buffer) {
	out << std::hex;
	for (std::size_t i = 0; i < 256; ++i) {
		if (i && (i % 8) == 0) out << "  ";
		if (i && (i % 16) == 0) out << std::endl;
		if (i % 8) out << " ";
		out << std::setfill('0') << std::setw(2) << std::hex << int(buffer[i] & 0xff);
	}
	return out << std::dec;
}
