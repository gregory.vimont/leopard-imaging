/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#include "device.hpp"

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <vector>

inline void
getUsage(const std::string& name) {
	std::cout << "usage: " << name << " [OPTIONS] COMMAND" << std::endl
	          << "OPTIONS:" << std::endl
	          << "-h, --help: Show usage and help" << std::endl
	          << "-v, --verbose: enable verbose log" << std::endl
	          << "-d, --device <name>: Specify device name (default: dev/video0)"
	          << std::endl
	          << "COMMAND:" << std::endl
	          << "-r <filename>: Write device firmware to <filename>" << std::endl
	          << "-c <img>: Compare <img> with the device firmware" << std::endl
	          << "-f <img>: Flash the device firmware with <img>" << std::endl
	          << "-v: get the device firmware version" << std::endl;
}

inline std::string
getCmdOption(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	std::vector<std::string>::const_iterator itr =
	  std::find(args.begin(), args.end(), option);
	if (itr != args.end() && ++itr != args.end()) {
		return *itr;
	}
	return std::string();
}

inline bool
isCmdExists(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	return std::find(args.begin(), args.end(), option) != args.end();
}

std::string
getDeviceName(int argc, char** argv) {
	std::string deviceName("/dev/video0");
	if (isCmdExists(argc, argv, "-d")) {
		deviceName = getCmdOption(argc, argv, "-d");
	} else if (isCmdExists(argc, argv, "--device")) {
		deviceName = getCmdOption(argc, argv, "--device");
	} else {
		std::cout << "(WARNING) no device specified." << std::endl;
	}
	std::cout << "(INFO) using device: " << deviceName << std::endl;
	return deviceName;
}

bool
getVerbosity(int argc, char** argv) {
	if (isCmdExists(argc, argv, "-v") || isCmdExists(argc, argv, "--verbose")) {
		return true;
	}
	return false;
}

enum class Command { READ, CHECK, FLASH, VERSION, INVALID };

std::pair<enum Command, std::string>
getCommand(int argc, char** argv) {
	std::pair<enum Command, std::string> result = std::make_pair(Command::INVALID, "");
	if (isCmdExists(argc, argv, "-r")) {
		result.first  = Command::READ;
		result.second = getCmdOption(argc, argv, "-r");
	}
	if (isCmdExists(argc, argv, "-c")) {
		if (result.first != Command::INVALID) {
			throw std::runtime_error("Can't use more than one command at a time");
		}
		result.first  = Command::CHECK;
		result.second = getCmdOption(argc, argv, "-c");
	}
	if (isCmdExists(argc, argv, "-f")) {
		if (result.first != Command::INVALID) {
			throw std::runtime_error("Can't use more than one command at a time");
		}
		result.first  = Command::FLASH;
		result.second = getCmdOption(argc, argv, "-f");
	}
	if (isCmdExists(argc, argv, "-v")) {
		if (result.first != Command::INVALID) {
			throw std::runtime_error("Can't use more than one command at a time");
		}
		result.first = Command::VERSION;
	}
	if (result.first == Command::INVALID) {
		throw std::runtime_error("Please select a command (read, check, flash, version).");
	}
	return result;
}

int
main(int argc, char* argv[]) {
	if (argc < 2 || isCmdExists(argc, argv, "-h") || isCmdExists(argc, argv, "--help")) {
		getUsage(argv[0]);
		return 0;
	}

	std::pair<Command, std::string> action = getCommand(argc, argv);
	std::string deviceName                 = getDeviceName(argc, argv);
	bool verbose                           = getVerbosity(argc, argv);

	Device device(deviceName, verbose);
	switch (action.first) {
		case Command::READ:
			device.readFirmware(action.second);
			break;
		case Command::CHECK:
			if (!device.checkFirmware(action.second)) {
				std::cerr << "(ERROR) Device and firmware file are differents !" << std::endl;
				return 2;
			} else {
				std::cout << "(INFO) Device and firmware file are identical." << std::endl;
			}
			break;
		case Command::FLASH:
			device.writeFirmware(action.second);
			if (!device.checkFirmware(action.second)) {
				std::cerr << "(ERROR) Device and firmware file are differents !" << std::endl;
				std::cerr << "(CRITICAL) Flash again and don't disconnect the device !"
				          << std::endl;
				return 3;
			} else {
				std::cout << "(INFO) Flashing Done, You can disconnect the device."
				          << std::endl;
			}
			break;
		case Command::VERSION:
			device.getFirmwareVersion();
			break;
		case Command::INVALID:
			throw std::runtime_error("Invalid command");
	}
	return 0;
}
