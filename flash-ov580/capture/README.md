# Description
This capture was taken with omnivision tool.

# Dump
To dump usb we use wireshark and usbmon.  
## usbmon
First verify usbmon is running:
```sh
lsmod | grep ^usbmon
```
If you see nothing, then use:
```sh
sudo modprobe usbmon
```
## wireshark
Now you can run wireshark
```sh
sudo wireshark
```
note: if working you should see graphic on usbmon
![capture](wireshark_capture.png)
note: using *lsusb* you can see oon which bus your device is connected thus you
can find the usbmon associated.

# Analyze of CBS Tool
To read the capture file, you'll need wireshark (wireshark-qt)
```sh
wireshark ov580-flash-read.pcapng
```
note: to only see command you can use the filter:
```
frame.len == 448
```

The differents specs seems to be:
0. Two undocumented commands are issued. 
1. It first erase the flash by 4k sector.
2. Then it flash the OV580SpecificFW20151229.bin.
(md5: 58ba158000ba8bdc14c13ba45bab5316)
3. Last step, the tools read the firmware flashed to
check integrity.

## Hidden commands
It seems that first, the tools send two hidden commands.

* First No. 219, we can see this undocumented command...  

|Byte|Description    |Value|
|:--:|---------------|:---:|
| 0  |Command ID     | 0x51|
| 1  |Command Type   | 0xA1|
| 2  |Command SubType| 0x39|

* Following by No. 227 which again use the command (sub type ?) 0x39...  

|Byte|Description    |Value|
|:--:|---------------|:---:|
| 0  |Command ID     | 0x51|
| 1  |Command Type   | 0xA1|
| 2  |Command SubType| 0x39|
| ~  |               |     |
| 8  |????           | 0x1C|

AFAIK, this commands are mandatory before trying to erase or write some pages.  

**-> If OV could explain the purpose of this two commands...**

## Completion Test

I also notice, that the CBS tools gets data from device (i.e. **UVC_GET**) after each
Read/Write/Errase command (e.g. No. 222, 224). Data seems to be equal to the
last *UVC_SET* command but shifted by one byte.

My supposition, is the byte 0 become a completion flag with:

|Value| Potential Meaning|
|:---:|------------------|
| 0x55| In Progress/Busy |
| 0x56| Done             |
| 0x54| Idle             |

So CBS Tool, seems to wait for 0x56 before issuing a new **UVC_SET** command...  

**-> If OV could explain this data shift and the meaning of the byte 0.**

After some investigation, I also notice:
**-> if program don't sleep for 5ms after write UVC_SET is issued, the write error increase significantly.**

