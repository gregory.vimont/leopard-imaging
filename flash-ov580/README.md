# Description
You can find here, a program to flash a new firmware for the leopard stereo camera LI-OV4689 aka OV4689x2 -> OV580 -> USB 3.0.  

# HowTo Build
On your host you can build using pure CMake build, for robot, you'll need qiBuild and do a cross compilation...  
note: Read doc in Toolchain directory for building using qiBuild.

## Dependencies
Project use CMake >= 3.2, C++11, and v4l2 API (i.e. programs are linux only).  
To install new cmake version on *old* Ubuntu just retrieve a new version on ppa.
```sh
sudo add-apt-repository ppa:george-edison55/cmake-3.x -y
sudo apt-get update -qq
sudo apt-get install -qq cmake
```

## Pure CMake build (Host)
On Desktop (i.e. native build):
```sh
mkdir build && cd build
cmake ..
make
```

## qiBuild way
### Desktop Build (Native build)
First you need to configure and build using qiBuild tools:
```sh
qibuild configure --release
qibuild make
```

### Cross build (Target Robot)
First you need to configure and build using qiBuild tools:
```sh
qibuild configure -c pepper --release
qibuild make -c pepper
```

Then, you can deploy on robot
```sh
qibuild deploy -c pepper --url nao@ip.of.the.robot:~/<target_destination_dir>
```

# HowTo Test
If you want program usage just call it without argument, like usual:
```sh
$ ./build-sys-linux-x86_64/sdk/bin/flash-ov580 
usage: ./build-sys-linux-x86_64/sdk/bin/flash-ov580 [OPTIONS] COMMAND
OPTIONS:
-h, --help: Show usage and help
-v, --verbose: enable verbose log
-d, --device <name>: Specify device name (default: dev/video0)
COMMAND:
-r <filename>: Write device firmware to <filename>
-c <img>: Compare <img> with the device firmware
-f <img>: Flash the device firmware with <img>
-v: get the device firmware version
```

## On Desktop
First you need to determine which /dev device correspond to your camera
```sh
for i in `ls -d /sys/class/video4linux/video*`; do echo "$i:"; cat ${i}/name ;done
```
Output example
```sh
[...]
/sys/class/video4linux/video1:
OV580 STEREO
```
So here stereo camera use **/dev/video1** ....

Then, once the project has been build, you can test it using:  
To compare Device firmware with a file:
```sh
./build/bin/flash-ov580 --verbose -d /dev/video1 -c OV580SpecificFW20160523.bin
```
To read the firmware by using:
```sh
./build/bin/flash-ov580 -d /dev/video1 -r dump.bin
```
To flash firmware by using:
```sh
./build/bin/flash-ov580 -d /dev/video1 -f OV580SpecificFW20160523.bin
```

## On Robot
On robot stereo camera will **always** be /dev/video-stereo.  
After having deployed program on robot.  
You can run it, using:
```sh
ssh nao@ip.of.the.robot
./<target_destination_dir>/bin/flash-ov580 --verbose -d /dev/video-stereo -c OV580SpecificFW20160523.bin
```

