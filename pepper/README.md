# Architecture
Pepper head contains three USB 3.0 Cameras on the same HUB.
* Two 2D USB 3.0 Cameras using a [CX3](https://www.e-consystems.com/CX3-Reference-Design-Kit.asp) and a [OV5640](http://www.ovt.com/products/sensor.php?id=177).
* One 3D USB 3.0 Cameras using an [OV580]() and two [OV4689](http://www.ovt.com/products/sensor.php?id=136).

## 2D Cameras
First a udev rule located at /etc/udev/rules.d/50-usb-cypress.rules is
responsible to upload the firmware
```sh
# - top camera
ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="04b4", ATTR{idProduct}=="0053|00f3", ATTR{busnum}=="1", ATTR{devpath}=="1.1", \
  RUN+="/usr/libexec/cx3_push_firmware $attr{busnum} $attr{devpath} /lib/firmware/CX3RDK_OV5640_20160705.img"
# - bottom camera
ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="04b4", ATTR{idProduct}=="0053|00f3", ATTR{busnum}=="1", ATTR{devpath}=="1.2", \
  RUN+="/usr/libexec/cx3_push_firmware $attr{busnum} $attr{devpath} /lib/firmware/CX3RDK_OV5640_20160705.img"
```

Then an udev rules located at /etc/udev/rules.d/99-aldebaran.rules is
responsible to create symlinks...
```sh
KERNEL=="video[0-9]*", SUBSYSTEM=="video4linux", KERNELS=="2-1.1", TAG+="systemd", SYMLINK+="video-top"
KERNEL=="video[0-9]*", SUBSYSTEM=="video4linux", KERNELS=="2-1.2", TAG+="systemd", SYMLINK+="video-bottom"
```

## 3D Camera
Since 3D camera boot as an UVC device we juste apply an udev rules to create a
symlink...
```sh
KERNEL=="video[0-9]*", SUBSYSTEM=="video4linux", KERNELS=="2-1.3", TAG+="systemd", SYMLINK+="video-stereo"
```

# Find Robot IP
Usually when you plug an ethernet cable on the head, Robot should automatically get
an IP address on your local network (connman manager use dhcpcd daemon like).

To retrieve this IP, the faster way is to use the mini usb port (like any 
android smartphone) which is in fact, a serial port (aka ttyUSB).

First install **minicom**
```sh
sudo apt-get update
sudo apt-get install minicom
```
You can use the following function to setup and launch minicom (i.e. to put in your .zshrc for example)
```sh
# Need minicom to use serial port on robot
ttyusb() {
  sudo ln -sf /dev/ttyUSB[0-9] /dev/ttyUSBX
  echo `ls -la /dev/ttyUSBX`
  sudo sh -c 'echo -e "pu port /dev/ttyUSBX\npu rtscts No" > /etc/minirc.dfl'
  sudo minicom --color=on
}
```

After you just need to tip:
```sh
ttyusb
```
Then enter your password (sudo need root access)
and you should be able to see a terminal on robot.

login: nao  
password: nao

Finally to retrieve the ip address just write, in the minicom terminal, the command:
```sh
ip a
```

# SSH to Robot
In an terminal simply use:
```sh
ssh nao@ip
```

# Streaming Video
To stream video from robot to your desktop, the simplest solution is to use
gstreamer on robot and vlc or gstreamer on your computer as follow...  
On Robot:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video-top ! video/x-raw-yuv,width=640,height=480,framerate=30/1 ! ffmpegcolorspace ! jpegenc ! multipartmux! tcpserversink port=3000 
gst-launch-0.10 -v v4l2src device=/dev/video-bottom ! video/x-raw-yuv,width=640,height=480,framerate=30/1 ! ffmpegcolorspace ! jpegenc ! multipartmux! tcpserversink port=3001 
gst-launch-0.10 -v v4l2src device=/dev/video-stereo ! video/x-raw-yuv,width=2560,height=720,framerate=15/1 ! ffmpegcolorspace ! jpegenc ! multipartmux! tcpserversink port=5000
```

On Desktop:
```sh
gst-launch-0.10 tcpclientsrc host=... port=3000 ! multipartdemux ! jpegdec ! videoflip method=2 ! autovideosink
gst-launch-0.10 tcpclientsrc host=... port=3001 ! multipartdemux ! jpegdec ! autovideosink
gst-launch-0.10 tcpclientsrc host=... port=5000 ! multipartdemux ! jpegdec ! autovideosink
```
note: replase *...* by your robot IP.  
Top Camera is mounted upside down so we use videoflip to flip the image...

Otherwise you can also use VLC (Media->Open Network Stream...->Network or in cli)
```sh
vlc tcp://...:5000
```

![Stereo Streaming Example](streaming_stereo.png)
