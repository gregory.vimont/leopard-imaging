//! @file
#include <gtest/gtest.h>

#include "tools.hxx"
#include <CameraLIOV5640.hpp>

//! @brief Stores the list of available Format for CameraLIOV5640 in USB 3.0.
const std::vector<FormatAF> g_FormatAFList = {{{{320, 240}, 15}, false},
                                              {{{320, 240}, 30}, false},
                                              {{{640, 480}, 15}, false},
                                              {{{640, 480}, 30}, false},
                                              {{{1280, 960}, 15}, false},
                                              {{{1280, 960}, 30}, false},
                                              {{{1920, 1080}, 15}, false},
                                              {{{1920, 1080}, 30}, false},
                                              {{{2592, 1944}, 15}, false},
                                              {{{320, 240}, 15}, true},
                                              {{{320, 240}, 30}, true},
                                              {{{640, 480}, 15}, true},
                                              {{{640, 480}, 30}, true},
                                              {{{1280, 960}, 15}, true},
                                              {{{1280, 960}, 30}, true},
                                              {{{1920, 1080}, 15}, true},
                                              {{{1920, 1080}, 30}, true},
                                              {{{2592, 1944}, 15}, true}};

//! @brief Instantiate CameraLIOV5640 format tests.
INSTANTIATE_TEST_CASE_P(Format,
                        SingleFormatAFTest,
                        ::testing::ValuesIn(g_FormatAFList));

//! @brief Generates exhaustive list of FormatAFToFormatAF possibilities.
//! @return a vector of all possible FormatAFToFormatAF.
std::vector<FormatAFToFormatAF>
generateFormatAFToFormatAFList() {
	std::vector<FormatAFToFormatAF> out;
	for (const auto& first : g_FormatAFList) {
		for (const auto& second : g_FormatAFList) {
			if (first == second) continue;
			out.push_back(std::make_pair(first, second));
		}
	}
	return out;
}

//! @brief Instantiate CameraLIOV5640 format to format tests.
INSTANTIATE_TEST_CASE_P(Format,
                        SwitchFormatAFTest,
                        ::testing::ValuesIn(generateFormatAFToFormatAFList()));
