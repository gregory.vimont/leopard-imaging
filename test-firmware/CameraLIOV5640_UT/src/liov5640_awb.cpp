//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 Auto White Balance info are correct.
TEST(AWB, CheckAWBAutoInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Auto White Balance Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_AUTO_WHITE_BALANCE));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 1);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, V4L2_WHITE_BALANCE_AUTO);
}

//! @test Checks CameraLIOV5640 White Balance Temperature info are correct.
TEST(AWB, CheckAWBTemperatureInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get White Balance Temperature Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_WHITE_BALANCE_TEMPERATURE));
	EXPECT_EQ(2500, info.min);
	EXPECT_EQ(6500, info.max);
	EXPECT_EQ(500, info.step);
	EXPECT_EQ(2500, info.def);
}

//! @todo Checks CameraLIOV5640 convergence take up to 15 frames when AEC enabled.

//! @test Checks CameraLIOV5640 White Balance getter and setter are working.
TEST(AWB, TestManualWhiteBalance) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Disable Auto White Balance");
	EXPECT_NO_THROW(
	  cam.setParameter(V4L2_CID_AUTO_WHITE_BALANCE, V4L2_WHITE_BALANCE_MANUAL));
	SCOPED_TRACE(cam.device() + ": Set White Balance (3000)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE, 3000));
	SCOPED_TRACE(cam.device() + ": Check White Balance == 3000");
	EXPECT_EQ(3000, cam.getParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE));

	SCOPED_TRACE(cam.device() + ": Start");
	ASSERT_NO_THROW(cam.start());

	SCOPED_TRACE(cam.device() + ": Check Auto White Balance");
	EXPECT_EQ(V4L2_WHITE_BALANCE_MANUAL, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check White Balance == 3000");
	EXPECT_EQ(3000, cam.getParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE));
	testCameraLIOV5640Stream("TestWhiteBalance3000", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Set White Balance (5000)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE, 5000));
	SCOPED_TRACE(cam.device() + ": Check White Balance == 5000");
	EXPECT_EQ(5000, cam.getParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE));
	testCameraLIOV5640Stream("TestWhiteBalance5000", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Auto White Balance");
	EXPECT_EQ(V4L2_WHITE_BALANCE_MANUAL, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check White Balance == 5000");
	EXPECT_EQ(5000, cam.getParameter(V4L2_CID_WHITE_BALANCE_TEMPERATURE));
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_AUTO_WHITE_BALANCE, 1));
}
