//! @file
#include "tools.hpp"

#include <CameraLIOV5640.hpp>
#include <string>
#include <thread> // sleep

extern bool g_enableDump;

FormatAF::FormatAF(Camera::Format format, bool af)
  : std::pair<Camera::Format, bool>(std::move(format), std::move(af)) {}

void
setupCameraLIOV5640(Camera& cam, const FormatAF& format) {
	SCOPED_TRACE(cam);
	SCOPED_TRACE(cam.device() + ": Open Device");
	ASSERT_NO_THROW(cam.openDevice());
	SCOPED_TRACE(cam.device() + ": Init Device");
	ASSERT_NO_THROW(cam.init());
	SCOPED_TRACE(cam.device() + (format.second == true ? ": Enable" : ": Disable") +
	             " AutoFocus");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, format.second));
	SCOPED_TRACE(cam.device() + ": Set Format: " + std::to_string(format.first));
	ASSERT_NO_THROW(cam.setFormat(format.first));
}

void
testCameraLIOV5640Stream(const std::string& testName,
                         Camera& cam,
                         const FormatAF& format,
                         bool enableDrop) {
	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	std::unique_ptr<Camera::Image> imgPtr;
	ASSERT_NO_THROW(imgPtr = cam.flushBuffers());

	SCOPED_TRACE(cam.device() + ": GetImage x" + std::to_string(IMG_LOOP));
	std::size_t defectCount = 0;
	for (std::size_t i = 0; i < IMG_LOOP; ++i) {
		SCOPED_TRACE(cam.device() + ": loop: " + std::to_string(i));
		EXPECT_NO_THROW(imgPtr = cam.getImage())
		  << "[ ERROR    ] NoImage, " << cam.device() << ", " << std::to_string(format)
		  << ", " << std::to_string(i);

		if (imgPtr) {
			SCOPED_TRACE(cam.device() + ": image seq: " + std::to_string(imgPtr->seq));
			// Compute last four byte value
			std::uint32_t imgValue = 0;
			{
				// Format YUV422 => 16bits/pixel
				std::uint32_t* ptr = reinterpret_cast<std::uint32_t*>(imgPtr->buffer.get());
				ptr += (cam.format().resolution.width * cam.format().resolution.height / 2) - 1;
				imgValue = *ptr;
			}
			if (imgValue == 0u) {
				defectCount++;
				ADD_FAILURE() << "[ ERROR    ] ImageCorrupted, " << cam.device() << ", "
				              << std::to_string(format) << ", " << std::to_string(i) << ", "
				              << std::to_string(imgPtr->seq);
				// Dump the first DEFECT_NB corrupted images.
				if (defectCount <= DEFECT_NB) {
					EXPECT_NO_THROW(
					  cam.writeImage2PPM(*imgPtr, testName + "_" + std::to_string(i) + ".ppm"));
				}
			}
		}
		if (enableDrop) {
			std::this_thread::sleep_for(
			  std::chrono::milliseconds(3000 / cam.format().frameRate));
		}
	}
	if (imgPtr && g_enableDump) {
		std::stringstream ss;
		ss << testName << "_" << std::to_string(IMG_LOOP) << ".ppm";
		SCOPED_TRACE(cam.device() + ": Write image " + ss.str());
		EXPECT_NO_THROW(cam.writeImage2PPM(*imgPtr, ss.str()));
	}
	std::cout << "[ INFO     ] DefectCount, " << cam.device() << ", "
	          << std::to_string(format) << ", " << std::to_string(defectCount)
	          << std::endl;
}

void
testCameraLIOV5640Stream(const std::string& testName,
                         Camera& cam,
                         const FormatAF& prevFormat,
                         const FormatAF& currFormat,
                         bool enableDrop) {
	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	std::unique_ptr<Camera::Image> imgPtr;
	ASSERT_NO_THROW(imgPtr = cam.flushBuffers());

	SCOPED_TRACE(cam.device() + ": GetImage x" + std::to_string(IMG_LOOP));
	std::size_t defectCount = 0;
	for (std::size_t i = 0; i < IMG_LOOP; ++i) {
		SCOPED_TRACE(cam.device() + ": loop: " + std::to_string(i));
		EXPECT_NO_THROW(imgPtr = cam.getImage())
		  << "[ ERROR    ] NoImage, " << cam.device() << ", " << std::to_string(currFormat)
		  << ", " << std::to_string(i);

		if (imgPtr) {
			SCOPED_TRACE(cam.device() + ": image seq: " + std::to_string(imgPtr->seq));
			// Compute last four byte value
			std::uint32_t imgValue = 0;
			{
				// Format YUV422 => 16bits/pixel
				std::uint32_t* ptr = reinterpret_cast<std::uint32_t*>(imgPtr->buffer.get());
				ptr += (cam.format().resolution.width * cam.format().resolution.height / 2) - 1;
				imgValue = *ptr;
			}
			if (imgValue == 0u) {
				defectCount++;
				ADD_FAILURE() << "[ ERROR    ] ImageCorrupted, " << cam.device() << ", "
				              << std::to_string(prevFormat) << ", "
				              << std::to_string(currFormat) << ", "
				              << std::to_string(imgPtr->seq);
				// Dump the first DEFECT_NB corrupted images.
				if (defectCount <= DEFECT_NB) {
					EXPECT_NO_THROW(
					  cam.writeImage2PPM(*imgPtr, testName + "_" + std::to_string(i) + ".ppm"));
				}
			}
		}
		if (enableDrop) {
			std::this_thread::sleep_for(
			  std::chrono::milliseconds(3000 / cam.format().frameRate));
		}
	}
	if (imgPtr && g_enableDump) {
		std::stringstream ss;
		ss << testName << "_" << std::to_string(IMG_LOOP) << ".ppm";
		SCOPED_TRACE(cam.device() + ": Write image " + ss.str());
		EXPECT_NO_THROW(cam.writeImage2PPM(*imgPtr, ss.str()));
	}
	std::cout << "[ INFO     ] DefectCount, " << cam.device() << ", "
	          << std::to_string(prevFormat) << ", " << std::to_string(currFormat) << ", "
	          << ", " << std::to_string(defectCount) << std::endl;
}

void
testCameraLIOV5640PatternStream(const std::string& testName,
                                Camera& cam,
                                const FormatAF& format,
                                bool enableDrop) {
	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush Stream");
	ASSERT_NO_THROW(cam.flushBuffers());

	SCOPED_TRACE(cam.device() + ": Enable Test Pattern");
	{
		// 0x86 Color Square, 0x96 Black&White square
		ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::TEST_PATTERN, 0x86));
	}

	SCOPED_TRACE(cam.device() + ": Build RefImage");
	std::unique_ptr<Camera::Image> refImgPtr;
	{
		for (std::size_t i = 0; i < 15; ++i) {
			ASSERT_NO_THROW(cam.getImage());
		}
		ASSERT_NO_THROW(refImgPtr = cam.getImage());
		if (g_enableDump) {
			std::stringstream ss;
			ss << testName << "_refImg.ppm";
			SCOPED_TRACE(cam.device() + ": Write image " + ss.str());
			EXPECT_NO_THROW(cam.writeImage2PPM(*refImgPtr, ss.str()));
		}
	}

	SCOPED_TRACE(cam.device() + ": GetImageAndCompare x" + std::to_string(IMG_LOOP));
	std::size_t defectCount = 0;
	std::unique_ptr<Camera::Image> imgPtr;
	for (std::size_t i = 0; i < IMG_LOOP; ++i) {
		SCOPED_TRACE(cam.device() + ": loop: " + std::to_string(i));
		EXPECT_NO_THROW(imgPtr = cam.getImage())
		  << "[ ERROR    ] NoImage, " << cam.device() << ", " << std::to_string(format)
		  << ", " << std::to_string(i);

		if (imgPtr) {
			SCOPED_TRACE(cam.device() + ": image seq: " + std::to_string(imgPtr->seq));
			std::size_t defectPixelCount = 0;
			bool failure                 = false;
			auto refDataPtr              = refImgPtr->buffer.get();
			auto imgDataPtr              = imgPtr->buffer.get();
			for (std::size_t y = 0; y < cam.format().resolution.height; ++y) {
				for (std::size_t x = 0; x < cam.format().resolution.width; ++x) {
					short valueRef = *refDataPtr++;
					short valueImg = *imgDataPtr++;
					// Compare Y component
					if (std::abs(valueRef - valueImg) > 0) {
//						ADD_FAILURE() << "[ ERROR    ] PixelCorrupted, " << cam.device() << ", "
//						              << std::to_string(format) << ", "
//						              << std::to_string(imgPtr->seq) << ", Y Pixel["
//						              << std::to_string(y) << "][" << std::to_string(x) << "], "
//						              << std::to_string(valueRef) << " vs "
//						              << std::to_string(valueImg);
						defectPixelCount++;
						failure = true;
					}

					valueRef = *refDataPtr++;
					valueImg = *imgDataPtr++;
					// Compare U or V component
					if (std::abs(valueRef - valueImg) > 0) {
//						ADD_FAILURE() << "[ ERROR    ] PixelCorrupted, " << cam.device() << ", "
//						              << std::to_string(format) << ", "
//						              << std::to_string(imgPtr->seq) << ", U/V Pixel["
//						              << std::to_string(y) << "][" << std::to_string(x) << "], "
//						              << std::to_string(valueRef) << " vs "
//						              << std::to_string(valueImg);
						defectPixelCount++;
						failure = true;
					}
				}
			}
			if (failure && defectCount < DEFECT_NB) {
				defectCount++;
				ADD_FAILURE() << "[ ERROR    ] PixelCorrupted, " << cam.device() << ", "
								  << std::to_string(format) << ", "
								  << std::to_string(imgPtr->seq);
				EXPECT_NO_THROW(
				  cam.writeImage2PPM(*imgPtr, testName + "_" + std::to_string(i) + ".ppm"));
			}
			std::cout << "[ INFO     ] DefectPixelCount, " << cam.device() << ", "
			          << std::to_string(format) << ", " << std::to_string(imgPtr->seq) << ", "
			          << std::to_string(defectPixelCount) << std::endl;
			// Current Image become refImg for the next one...
			refImgPtr = std::move(imgPtr);
		}
		if (enableDrop) {
			std::this_thread::sleep_for(
			  std::chrono::milliseconds(3000 / cam.format().frameRate));
		}
	}

	std::cout << "[ INFO     ] DefectCount, " << cam.device() << ", "
	          << std::to_string(format) << ", " << std::to_string(defectCount)
	          << std::endl;
}
