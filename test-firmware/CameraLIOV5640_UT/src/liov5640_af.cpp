//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 Focus Absolute info are correct.
TEST(AF, CheckAFCAutoInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Auto Focus Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_FOCUS_AUTO));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(1, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(0, info.def);
}

//! @test Checks CameraLIOV5640 Focus Absolute info are correct.
TEST(AF, CheckAFCFocusInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Focus Absolute Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_FOCUS_ABSOLUTE));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(250, info.max);
	EXPECT_EQ(25, info.step);
	EXPECT_EQ(0, info.def);
}

//! @test Checks CameraLIOV5640 manual focus getter and setter are working.
TEST(AF, TestManualFocus) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Disable Auto Focus");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, 0));
	SCOPED_TRACE(cam.device() + ": Set Focus (25)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_ABSOLUTE, 25));
	SCOPED_TRACE(cam.device() + ": Check Focus == 25");
	EXPECT_EQ(25, cam.getParameter(V4L2_CID_FOCUS_ABSOLUTE));

	SCOPED_TRACE(cam.device() + ": Start");
	ASSERT_NO_THROW(cam.start());

	SCOPED_TRACE(cam.device() + ": Check Auto Focus");
	EXPECT_EQ(0, cam.getParameter(V4L2_CID_FOCUS_AUTO));
	SCOPED_TRACE(cam.device() + ": Check Focus == 25");
	EXPECT_EQ(25, cam.getParameter(V4L2_CID_FOCUS_ABSOLUTE));
	testCameraLIOV5640Stream("TestFocus25", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Set Focus (150)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_ABSOLUTE, 150));
	SCOPED_TRACE(cam.device() + ": Check Focus == 150");
	EXPECT_EQ(150, cam.getParameter(V4L2_CID_FOCUS_ABSOLUTE));
	testCameraLIOV5640Stream("TestFocus150", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Auto Focus");
	EXPECT_EQ(0, cam.getParameter(V4L2_CID_FOCUS_AUTO));
	SCOPED_TRACE(cam.device() + ": Check Focus == 150");
	EXPECT_EQ(150, cam.getParameter(V4L2_CID_FOCUS_ABSOLUTE));
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, 0));
}

//! @test Checks CameraLIOV5640 single trig auto focus is working.
TEST(AF, TestOneTrigFocus) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Disable Auto Focus");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, 0));
	SCOPED_TRACE(cam.device() + ": Set Focus to Single Trigg (250)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_ABSOLUTE, 250));
	SCOPED_TRACE(cam.device() + ": Check Focus == 250");
	EXPECT_EQ(250, cam.getParameter(V4L2_CID_FOCUS_ABSOLUTE));

	SCOPED_TRACE(cam.device() + ": Start");
	testCameraLIOV5640Stream("TestOneTrigFocus", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop");
	ASSERT_NO_THROW(cam.stop());
}
