//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 Brightness info are correct.
TEST(Controls, CheckAverageLuminanceInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Average Luminance Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getExtUnitInfo(CameraLIOV5640::AVERAGE_LUMINANCE));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 255);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 0);
}

//! @test Checks CameraLIOV5640 Horizontal Flip info are correct.
TEST(Controls, CheckHorizontalFlipInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Horizontal Flip Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getExtUnitInfo(CameraLIOV5640::HORIZONTAL_FLIP));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 1);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 0);
}

//! @test Checks CameraLIOV5640 Vertical Flip info are correct.
TEST(Controls, CheckVerticalFlipInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Vertical Flip Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getExtUnitInfo(CameraLIOV5640::VERTICAL_FLIP));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 1);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 0);
}

//! @test Checks CameraLIOV5640 extension units are robust.
TEST(Controls, CheckExtensionUnit) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	std::stringstream ss;
	for (int loop = 0; loop < 500; ++loop) {
		SCOPED_TRACE("loop: " + std::to_string(loop));
		SCOPED_TRACE(cam.device() + ": Open Device");
		ASSERT_NO_THROW(cam.openDevice());

		const std::vector<int> regs = {CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP,
		                               CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP,
		                               CameraLIOV5640::ExtensionUnit::TEST_PATTERN};
		for (const auto it : regs) {
			ss.str("");
			ss.clear();
			ss << cam.device() + ": Get ExtensionUnit: 0x" << std::hex << it;
			SCOPED_TRACE(ss.str());
			std::int32_t value;
			EXPECT_NO_THROW(value = cam.getExtUnit(it));
			ss.str("");
			ss.clear();
			ss << cam.device() << ": Set ExtensionUnit: 0x" << std::hex << it;
			SCOPED_TRACE(ss.str());
			EXPECT_NO_THROW(cam.setExtUnit(it, value));
		}
		ss.str("");
		ss.clear();
		ss << cam.device() << ": Get ExtensionUnit: 0x" << std::hex
		   << CameraLIOV5640::ExtensionUnit::AVERAGE_LUMINANCE;
		SCOPED_TRACE(ss.str());
		EXPECT_NO_THROW(
		  cam.getExtUnit(CameraLIOV5640::ExtensionUnit::AVERAGE_LUMINANCE)); // read only
		SCOPED_TRACE(cam.device() + ": Close Device");
		ASSERT_NO_THROW(cam.closeDevice());
	}
}
