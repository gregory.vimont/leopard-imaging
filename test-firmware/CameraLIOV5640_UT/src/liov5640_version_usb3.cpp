//! @file
#include <gtest/gtest.h>

#include <CameraLIOV5640.hpp>

#include "tools.hpp"
extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 USB 3.0 firmware revision.
TEST(Version, CheckRevision) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Version");
	std::uint32_t version;
	ASSERT_NO_THROW(version = cam.getVersion());
	EXPECT_EQ(20u, version);
}
