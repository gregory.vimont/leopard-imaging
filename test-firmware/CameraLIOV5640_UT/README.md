# Camera LI-OV5640 Test Plan
[TOC]

# Introduction {#LIOV5640_Intro}
This test plan is for 2D Camera LI-OV5640.  

All automatic tests are split in several categories:  
* [Auto Exposure Controls](#LIOV5640_AEC) test.  
* [Auto White Balance Controls](#LIOV5640_AWB) test.  
* [Auto Focus Controls](#LIOV5640_AF) test.  
* [Generic Controls](#LIOV5640_CONTROLS) test.  
* [Extension Unit Controls](#LIOV5640_EXTUNITS) test.  
* [Format and Streaming](#LIOV5640_FORMATS) test.  
* [Version](#LIOV5640_VERSION) test.  
Please note, some tests could need some visual check (use --dump option).

There is also few manual tests:  
* [Auto Focus](#LIOV5640_MAF) test.  
* [Hue](#LIOV5640_MHUE) test.  

## Binaries {#LIOV5640_Intro_bin}
Currently two binaries are provided depending if you are testing USB 2.0
or USB 3.0 firmware (e.g. Format support are differents thus test also).  

- "CameraLIOV5640_UT_USB2": is use for USB 2 firmware test.  
- "CameraLIOV5640_UT_USB3": is use for USB 3 firmware test.

If you are using pure CMake build, binary should be located in @b \<builddir\>/bin/.  
If you are using qibuild, binary should be located in @b \<builddir\>/sdk/bin/.  

## Testing {#LIOV5640_Intro_test}
note: In the rest of this document, I'll use the usb 3 binary, don't hesitate to
adapt command for usb 2 test.

To get some help
```sh
./CameraLIOV5640_UT_USB3 -h
```

To run all test:  
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video0
```  

## Options {#LIOV5640_Intro_misc}
To write image (in PPM P6 format) generated during test plan you can use **--dump** option.  

To get more verbosity, you can use **--verbose** or **-v** option.  
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video0 -v --dump > test.log 2>&1
```  
# Auto Exposure Control Test Plan {#LIOV5640_AEC}
## Description {#LIOV5640_AEC_Intro}

Currently on some robots:
- AEC take too much time (more than 15 frames) to converge.  
To compute the Illuminance, I compute the means of Y component on all pixel
(without using any weight !)
- AEC seems to be forced at startup whatever the state of ```exposure_auto```
 controls.

## Testing Convergence {#LIOV5640_AEC_Convergence}

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest-filter="AEC.*Convergence"
```

Here the sequence diagram.  
![Convergence](doc/LIOV5640_AEC_Convergence.png)
@image latex doc/LIOV5640_AEC_Convergence.png

## Testing Manual Exposure {#LIOV5640_AEC_Manual}

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest-filter="AEC.*Manual*"
```

Here the sequence diagram.  
![Manual](doc/LIOV5640_AEC_Manual.png)
@image latex doc/LIOV5640_AEC_Manual.png

# Auto White Balance Test Plan {#LIOV5640_AWB}
## Description {#LIOV5640_AWB_Intro}
Verify AWB Control.

You can test for ov5640 on robot head with:
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AWB*"
```

## Check Tests {#LIOV5640_AWB_Check}
Check tests will verify controls info (i.e. min, max, step and default value).  

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AWB.Check*"
```

Here the sequence diagram.  
![check](doc/LIOV5640_AWB_Check.png)
@image latex doc/LIOV5640_AWB_Check.png

## Stability Tests {#LIOV5640_AWB_Test}
Will test if cantrol can be changed whatever the camera state is and if state
change don't modify value.  

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AWB.Test*"
```

Here the sequence diagram.  
![test](doc/LIOV5640_AWB_Tests.png)
@image latex doc/LIOV5640_AWB_Tests.png

# Auto Focus Test Plan {#LIOV5640_AF}
## Description {#LIOV5640_AF_Intro}
Verify AF Control.

You can test for ov5640 on robot head with:
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AF*"
```

## Check Tests {#LIOV5640_AF_Check}
Check tests will verify controls info (i.e. min, max, step and default value).  

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AF.Check*"
```

Here the sequence diagram.  
![check](doc/LIOV5640_AF_Check.png)
@image latex doc/LIOV5640_AF_Check.png

## Stability Tests {#LIOV5640_AF_Test}
Will test if cantrol can be changed whatever the camera state is and if state
change don't modify value.  

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="AF.Test*"
```

Here the sequence diagram.  
![test](doc/LIOV5640_AF_Tests.png)
@image latex doc/LIOV5640_AF_Tests.png

# Controls Test Plan {#LIOV5640_CONTROLS}
## Description {#LIOV5640_CONTROLS_Intro}
Verify Basic Controls which are:
Brightness, Contrast, Hue, Saturation, Horizontal Flip and Vertical Flip...

You can test for ov5640 on robot head with:
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="Controls*"
```

## Check Tests {#LIOV5640_CONTROLS_Check}
Check tests will verify controls info (i.e. min, max, step and default value).  

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="Controls.Check*"
```

Here the sequence diagram.  
![check](doc/LIOV5640_Controls_Check.png)
@image latex doc/LIOV5640_Controls_Check.png

## Stability Tests {#LIOV5640_CONTROLS_Test}
Will test if cantrol can be changed whatever the camera state is and if state
change don't modify value.  
You can use the tag "[Stability]"

```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="Controls.Test*"
```

Here the sequence diagram.  
![test](doc/LIOV5640_Controls_Tests.png)
@image latex doc/LIOV5640_Controls_Tests.png

# Extension Unit Test Plan {#LIOV5640_EXTUNITS}
## Description {#LIOV5640_EXTUNITS_Intro}
Verify robustness of Extension Units.

This test will querry 500 times ramdomly some extension unit controls (read and write access).  
When a query fails, a log is printed.

## How to reproduce {#LIOV5640_EXTUNITS_Test}

Execute the test program with the video device to test:
```sh
$ ./CameraLIOV5640_UT_USB3 -d /dev/video0 --gtest_filter="*CheckExtensionUnit*"
```

Depending on the errors the kernel show broken pipe errors (-32) or connection timed out errors (-110) like:
```sh
$ dmesg | tail
[691095.497035] uvcvideo: Failed to query (SET_CUR) UVC control 10 on unit 3: -32 (exp. 2).
[691215.774805] uvcvideo: Failed to query (SET_CUR) UVC control 12 on unit 3: -32 (exp. 2).
[691237.283248] uvcvideo: Failed to query (SET_CUR) UVC control 12 on unit 3: -110 (exp. 2).
[691237.286855] uvcvideo: Failed to query (GET_CUR) UVC control 12 on unit 3: -71 (exp. 2).
[691245.476993] uvcvideo: Failed to query (SET_CUR) UVC control 10 on unit 3: -32 (exp. 2).
[691263.684537] uvcvideo: Failed to query (GET_CUR) UVC control 12 on unit 3: -32 (exp. 2).
[691314.602783] uvcvideo: Failed to query (SET_CUR) UVC control 12 on unit 3: -32 (exp. 2).
[691314.803909] uvcvideo: Failed to query (SET_CUR) UVC control 10 on unit 3: -32 (exp. 2).
[691315.000640] uvcvideo: Failed to query (GET_CUR) UVC control 13 on unit 3: -32 (exp. 2).
```

note: Errors "uvcvideo: Non-zero status (-71) in video completion handler." 
should be ignored.

# Format Test Plan {#LIOV5640_FORMATS}
## Description {#LIOV5640_FORMATS_Intro}
Verify Streaming Performance and Format switch functionalities.
You can test using the pattern.
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="Format*"
```

## Single Format Test {#LIOV5640_FORMATS_SingleTest}
### Verify Start Latency {#LIOV5640_FORMATS_SingleTest_Latency}
Verify if the latency to get first image after STREAM_ON is less than 1500ms.

Here the sequence diagram.  
![test_plan](doc/LIOV5640_Formats_StartLatency.png)
@image latex doc/LIOV5640_Formats_StartLatency.png

### Check Image Corruption {#LIOV5640_FORMATS_SingleTest_Corruption}
Verify if images are corrupted.

Here the sequence diagram.  
![test_plan](doc/LIOV5640_Formats_CheckImage.png)
@image latex doc/LIOV5640_Formats_CheckImage.png

Here the sequence diagram.  
![test_plan](doc/LIOV5640_Formats_CheckImageWithDrop.png)
@image latex doc/LIOV5640_Formats_CheckImageWithDrop.png

### Check Period Regularity {#LIOV5640_FORMATS_SingleTest_Regularity}
Verify if period is stable.

Here the sequence diagram.  
![test_plan](doc/LIOV5640_Formats_Regularity.png)
@image latex doc/LIOV5640_Formats_Regularity.png

## Multiple Format Test {#LIOV5640_FORMATS_MultiTest}

Here the sequence diagram.  
![test_plan](doc/LIOV5640_Formats_Switch.png)
@image latex doc/LIOV5640_Formats_Switch.png

# Version Test Plan {#LIOV5640_VERSION}
## Description {#LIOV5640_VERSION_Intro}
Retrieve the firmware version.

## Testing {#LIOV5640_VERSION_Test}
Verify if the firmware loaded is the last one.
```sh
./CameraLIOV5640_UT_USB3 -d /dev/video-top --gtest_filter="Version*"
```

note: each time a new firmware is available, this test must be updated !
# Auto-Focus Manual Test Plan {#LIOV5640_MAF}
## Description {#LIOV5640_MAF_Intro}
Verify Auto-Focus and Manual Focus functionalities using the following @b manual test plan.

## Auto focus {#LIOV5640_MAF_Auto}
* First start a streamn using gstreamer-0.10:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video0 ! video/x-raw-yuv,width=640,height=480,frame-rate=30 ! ffmpegcolorspace ! autovideosink
```
or in gstreamer-1.0:
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! autovideosink
```

* Then in an other terminal:
1. Set auto Focus mode:
```sh
v4l2-ctl -d /dev/video0 -c focus_auto=1
```
2. Verify auto focus is working...

## Manual Focus {#LIOV5640_MAF_Manual}
* First start a streamn using gstreamer-0.10:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video0 ! video/x-raw-yuv,width=640,height=480,frame-rate=30 ! ffmpegcolorspace ! autovideosink
```
or in gstreamer-1.0:
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! autovideosink
```

* Then in an other terminal:
1. Disable auto-focus using v4l2-ctl:
```sh
v4l2-ctl -d /dev/video0 -c focus_auto=0
```
2. Set out of focus value:
```sh
v4l2-ctl -d /dev/video0 -c focus_absolute=200
```
3. Verify auto focus is never triggered...

## Single Trig Auto-Focus {#LIOV5640_MAF_Single}
* First start a streamn using gstreamer-0.10:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video0 ! video/x-raw-yuv,width=640,height=480,frame-rate=30 ! ffmpegcolorspace ! autovideosink
```
or in gstreamer-1.0:
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! autovideosink
```

* Then in an other terminal:
1. Disable auto-focus using v4l2-ctl:
```sh
v4l2-ctl -d /dev/video0 -c focus_auto=0
```
2. Set out of focus value:
```sh
v4l2-ctl -d /dev/video0 -c focus_absolute=200
```
3. Verify auto focus is never triggered...
4. Trig one time auto-focus using 250 value:
```sh
v4l2-ctl -d /dev/video0 -c focus_absolute=250
```
5. Verify auto focus is never triggered again...

# Hue Manual Test Plan {#LIOV5640_MHUE}
You can test using the following test plans.
## Description {#LIOV5640_MHUE_Intro}
Verify Hue stability.

## Testing Hue {#LIOV5640_MHUE_Test}
You can test using the following test plans.

* First start a streamn using gstreamer-0.10:
```sh
gst-launch-0.10 -v v4l2src device=/dev/video0 ! video/x-raw-yuv,width=640,height=480,frame-rate=30 ! ffmpegcolorspace ! autovideosink
```
or in gstreamer-1.0:
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! autovideosink
```

* Then in an other terminal:
Set Hue to 0:
```sh
v4l2-ctl -d /dev/video0 -c hue=0
```
Since 0 is the default value, you should see no change.
Then try to set a weird value (e.g. 60)
```sh
v4l2-ctl -d /dev/video0 -c hue=60
```
You should see weird colors.

* Stop stream and restart it:
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! autovideosink
```
You should still have weird color...

* Stop stream, and restart it with a different resolution
```sh
gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=320,height=240,framerate=30/1 ! videoconvert ! autovideosink
```
You should still have weird colors.
Check Hue is still 60
```sh
v4l2-ctl -d /dev/video0 -C hue
```

* Set back Hue to 0:
```sh
v4l2-ctl -d /dev/video0 -c hue=0
```
You should, now, have the correct colors.

Side note: what is the purpose of **hue_auto**
```sh
v4l2-ctl -L
```
