# Camera LI-OV4689 Debug Tool
# Description
Read and write any register ISP or Sensor.  
note: prefix address with *0x* to write it in hexadecimal.

# ISP Register
## Read
To read for example version firmware simply type:
```sh
./ov4689_dbg -d /dev/video-stereo -r 0x8018fff0 -isp
Read ISP Address: 0x8018fff0 Value: 0x5
./ov4689_dbg -d /dev/video-stereo -r 0x8018fff1 -isp
Read ISP Address: 0x8018fff1 Value: 0x23
```

## Write
To write simply type:
```sh
./ov4689_dbg -d /dev/video-stereo -s addr value -isp
```

# Sensor Register
## Read
To read 0x5000 of the first sensor (SCCB0) simply type:
```sh
./ov4689_dbg -d /dev/video0 -sensor 0 -r 0x5000
Read Sensor: SCCB0 Address: 0x5000 Value: 0xf3
```

## Write
To write the first sensor (SCCB0) register simply type:
```sh
./ov4689_dbg -sensor 0 -s 0x5000 0xf0
Write Sensor: SCCB0 Address: 0x5000 Value: 0xf0
```

