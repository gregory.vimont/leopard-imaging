//! @file
#include <CameraLIOV4689.hpp>
#include <Tools.hpp>
#include <iostream>

//! @brief Default device file of the camera.
std::string g_device = "/dev/video0";
//! @brief Default Format to use.
Camera::Format g_preference = {{2560, 720}, 15};
//! @brief Enable camera verbose log.
bool g_enableVerbose = false;

//! @brief Display short help of the program.
//! @param[in] programName Name of the program (i.e. argv[0]).
static inline void
getUsage(const std::string& programName) {
	std::cout << "usage: " << programName << " -d /dev/video0 [...]" << std::endl
	          << "-d, --device <dev>: device name (default /dev/video0)" << std::endl
	          << "-isp: use ISP aka OV580 register" << std::endl
	          << "-sensor <id>: use Sensor register (id 0 or 1)" << std::endl
	          << "-r <addr>: read at address <addr>" << std::endl
	          << "-s <addr> <value>: write at address <addr> the value <value>"
	          << std::endl;
}

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int
main(int argc, char* argv[]) {
	if (argc < 2 ||
	    (!isCmdOptionExists(argc, argv, "-r") && !isCmdOptionExists(argc, argv, "-s"))) {
		getUsage(argv[0]);
		return 1;
	}

	if (isCmdOptionExists(argc, argv, "-d")) {
		g_device = getCmdOption(argc, argv, "-d");
	} else if (isCmdOptionExists(argc, argv, "--device")) {
		g_device = getCmdOption(argc, argv, "--device");
	}

	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	cam.openDevice();
	if (isCmdOptionExists(argc, argv, "-isp") &&
	    isCmdOptionExists(argc, argv, "-sensor")) {
		std::cerr
		  << "Can't use \"-isp\" and \"-sensor\" at the same time, please see usage."
		  << std::endl;
		return 2;
	}
	if (!isCmdOptionExists(argc, argv, "-isp") &&
	    !isCmdOptionExists(argc, argv, "-sensor")) {
		std::cerr << "Must specify isp or sensor, please see usage." << std::endl;
		return 2;
	}

	CameraLIOV4689::Sensor sensor = CameraLIOV4689::Sensor::SCCB0;
	if (isCmdOptionExists(argc, argv, "-sensor")) {
		if (std::string("0") == getCmdOption(argc, argv, "-sensor")) {
			sensor = CameraLIOV4689::Sensor::SCCB0;
		} else if (std::string("1") == getCmdOption(argc, argv, "-sensor")) {
			sensor = CameraLIOV4689::Sensor::SCCB1;
		} else {
			std::cerr << "Sensor must be 0 or 1, please see usage." << std::endl;
			return 3;
		}
	}

	if (isCmdOptionExists(argc, argv, "-r")) {
		std::uint32_t address = std::stoul(getCmdOption(argc, argv, "-r"), 0, 0);

		if (isCmdOptionExists(argc, argv, "-isp")) {
			std::cout << "Read ISP Address: 0x" << std::hex << address << " Value: 0x"
			          << std::uint32_t(cam.readISPRegister(address)) << std::dec << std::endl;
		} else {
			std::cout << "Read Sensor: " << sensor << " Address: 0x" << std::hex << address
			          << " Value: 0x"
			          << std::uint32_t(cam.readSensorRegister(sensor, address)) << std::dec
			          << std::endl;
		}
	} else if (isCmdOptionExists(argc, argv, "-s")) {
		std::uint32_t address = std::stoul(getCmdOption(argc, argv, "-s"), 0, 0);
		std::uint8_t value    = std::stoi(argv[getCmdIndex(argc, argv, "-s") + 2], 0, 0);

		if (isCmdOptionExists(argc, argv, "-isp")) {
			std::cout << "Write ISP Address: 0x" << std::hex << address << " Value: 0x"
			          << std::uint32_t(value) << std::dec << std::endl;
			cam.writeISPRegister(address, value);
		} else {
			std::cout << "Write Sensor: " << sensor << " Address: 0x" << std::hex << address
			          << " Value: 0x" << std::uint32_t(value) << std::dec << std::endl;
			cam.writeSensorRegister(sensor, address, value);
		}
	} else {
		std::cerr << "Must specify \"-r\" or \"-s\"." << std::endl;
		return 1;
	}
}
