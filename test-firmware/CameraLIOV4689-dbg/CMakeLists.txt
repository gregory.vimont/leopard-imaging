# Author(s):
#  - Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
# Copyright (C) 2016 SoftBank Robotics
cmake_minimum_required(VERSION 3.2)
project(CameraLIOV4689-dbg)

file(GLOB_RECURSE _SRCS "src/*.[hc]pp")
if(ENABLE_QIBUILD)
  qi_create_bin(${PROJECT_NAME} ${_SRCS})
  qi_use_lib(${PROJECT_NAME} camera)
else()
  add_executable(${PROJECT_NAME} ${_SRCS})
  target_link_libraries(${PROJECT_NAME} Camera)
  install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)
endif()
