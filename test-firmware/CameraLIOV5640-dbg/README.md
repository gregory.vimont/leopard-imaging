# Camera LI-OV5640 Debug Tool
# Description
Read and write any register.  
note: prefix address with *0x* to write it in hexadecimal.

# Camera Register
## Read
To read simply type:
```sh
./ov5640_dbg -d /dev/videoX -r addr
```

# Write
To read simply type:
```sh
./ov5640_dbg -d /dev/videoX -s addr value
```

