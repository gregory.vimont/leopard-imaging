//! @file

#include <CameraLIOV5640.hpp>
#include <Tools.hpp>
#include <iostream>
#include <map>

//! @brief Default device file of the camera.
std::string g_device = "/dev/video0";
//! @brief Default Format to use.
Camera::Format g_preference = {{640, 480}, 15};
//! @brief Enable camera verbose log.
bool g_enableVerbose = false;

//! @brief Display short help of the program.
//! @param[in] programName Name of the program (i.e. argv[0]).
static inline void
getUsage(const std::string& programName) {
	std::cout << "usage: " << programName << " -d /dev/video0 [...]" << std::endl
	          << "-d, --device <dev>: device name (default /dev/video0)" << std::endl
	          << "-r <addr>: read at address <addr>" << std::endl
	          << "-w <addr> <value>: write at address <addr> the value <value>"
	          << std::endl
	          << "-t <mode>: Activate test pattern " << std::endl
	          << "  mode among: none, color, vertical1, vertical2, horizontal"
	          << std::endl;
}

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int
main(int argc, char* argv[]) {
	{
		int cmd = 0;
		if (isCmdOptionExists(argc, argv, "-r")) cmd++;
		if (isCmdOptionExists(argc, argv, "-w")) cmd++;
		if (isCmdOptionExists(argc, argv, "-t")) cmd++;

		// Strictly one command at a time
		if (argc < 2 || cmd != 1) {
			getUsage(argv[0]);
			return 1;
		}
	}

	// Get Device
	if (isCmdOptionExists(argc, argv, "-d")) {
		g_device = getCmdOption(argc, argv, "-d");
	} else if (isCmdOptionExists(argc, argv, "--device")) {
		g_device = getCmdOption(argc, argv, "--device");
	}

	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	cam.openDevice();
	if (isCmdOptionExists(argc, argv, "-r")) {
		std::uint16_t address = std::stoi(getCmdOption(argc, argv, "-r"), 0, 0);
		std::uint16_t value   = cam.readRegister(address);
		std::cout << "Read Address: 0x" << std::hex << address << " Value: 0x" << value
		          << std::dec << " (" << value << ")" << std::endl;
	} else if (isCmdOptionExists(argc, argv, "-w")) {
		std::uint16_t address = std::stoi(getCmdOption(argc, argv, "-w"), 0, 0);
		std::uint16_t value   = std::stoi(argv[getCmdIndex(argc, argv, "-w") + 2], 0, 0);
		std::cout << "Write Address: 0x" << std::hex << address << " Value: 0x" << value
		          << std::dec << " (" << value << ")" << std::endl;
		cam.writeRegister(address, value);
	} else {
		std::string mode                            = getCmdOption(argc, argv, "-t");
		std::map<std::string, std::int16_t> modeMap = {{"none", 0},
		                                               {"color", 0x80},
		                                               {"vertical1", 0x84},
		                                               {"horizontal", 0x88},
		                                               {"vertical2", 0x8c}};
		std::int16_t value                          = 0;
		try {
			value = modeMap.at(mode);
		} catch (const std::out_of_range&) {
			std::cerr << "Mode unknow !" << std::endl;
			getUsage(argv[0]);
			return 2;
		}

		std::cout << "Test Pattern, set mode: " << mode << " Value: 0x" << std::hex << value
		          << std::dec << " (" << value << ")" << std::endl;
		cam.setExtUnit(CameraLIOV5640::TEST_PATTERN, value);
	}
}
