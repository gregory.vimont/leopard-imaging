//! @file
#pragma once

#include <QScrollArea>

class QLabel;

//! @brief Provides Camera streaming view.
class ImageViewer : public QScrollArea {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	ImageViewer(const ImageViewer&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	ImageViewer& operator=(const ImageViewer&) = delete;
	//! @brief Default Constructor.
	//! @param[in] parent Parent Widget if any.
	ImageViewer(QWidget* parent = 0);

	virtual ~ImageViewer() = default;
	//! @brief Gets the recommended size for the widget.
	//! @return the recommended size.
	virtual QSize sizeHint() const override;
	//! @brief Gets the recommended minimum size for the widget.
	//! @return the recommended minimum size.
	virtual QSize minimumSizeHint() const override;

	public slots:
	//! @brief Updates view with the new image.
	//! @param[in] img The image to display.
	void onImage(QImage img);

	protected:
	//! @brief Event handler when mouse is pressd.
	//! @param[in] event Mouse event parameters.
	virtual void mousePressEvent(QMouseEvent* event);
	//! @brief Event handler when mouse is release.
	//! @param[in] event Mouse event parameters.
	virtual void mouseReleaseEvent(QMouseEvent* event);
	//! @brief Event handler when mouse move.
	//! @details Use to drag image while left button is pressed.
	//! @param[in] event Mouse event parameters.
	virtual void mouseMoveEvent(QMouseEvent* event);
	//! @brief Event handler when wheel move.
	//! @param[in] event Mouse wheel event parameters.
	virtual void wheelEvent(QWheelEvent* event);
	//! @brief Event handler when key is pressed.
	//! @details Space will reset the zoom level.
	//! @param[in] event Key event parameters.
	virtual void keyPressEvent(QKeyEvent* event);

	//! @brief Stores if the mouse left button is pressed.
	bool _mouseLeft;
	//! @brief Stores the previous mouse X position.
	int _mouseX;
	//! @brief Stores the previous mouse Y position.
	int _mouseY;

	//! @brief Stores the image to display.
	QImage _image;
	//! @brief Stores the scale factor use to render the image.
	float _scaleFactor;

	private:
	//! @brief Displays statistic about the image.
	QLabel* _imageLabel;

	//! @brief Zooms in the Image.
	void zoomIn();
	//! @brief Zooms out the Image.
	void zoomOut();

	//! @brief Initializes the widget.
	void setupGUI();
};
