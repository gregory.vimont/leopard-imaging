//! @file
#include "CameraControls.hpp"

#include "ExposureMeteringControls.hpp"
#include "RegisterControls.hpp"
#include "StreamControls.hpp"
#include <QGroupBox>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

CameraControls::CameraControls(Camera& camera, QWidget* parent)
  : QWidget(parent)
  , _camera(camera)
  , _streamControls(nullptr)
  , _registerControls(nullptr) {
	setupGUI();
}

void
CameraControls::onConfigure() {
	_streamControls->onConfigure();
}

void
CameraControls::onRefresh() {
	_streamControls->onRefresh();
}

void
CameraControls::setupGUI() {
	setObjectName(QString::fromUtf8("CameraControls"));
	setLayout(new QVBoxLayout(this));
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

	QTabWidget* tabWidget = new QTabWidget();
	_streamControls       = new StreamControls(_camera);
	tabWidget->addTab(_streamControls, _streamControls->objectName());

	_registerControls = new RegisterControls(_camera);
	tabWidget->addTab(_registerControls, _registerControls->objectName());

	_exposureMeteringControls = new ExposureMeteringControls(_camera);
	tabWidget->addTab(_exposureMeteringControls, _exposureMeteringControls->objectName());

	layout()->addWidget(tabWidget);
}
