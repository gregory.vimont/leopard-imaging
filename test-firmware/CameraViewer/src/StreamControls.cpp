//! @file
#include "StreamControls.hpp"

#include <QComboBox>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

StreamControls::StreamControls(Camera& camera, QWidget* parent)
  : QWidget(parent)
  , _camera(camera) {
	setupGUI();
}

void
StreamControls::onRefresh() {
	try {
		bool open          = _camera.isOpen();
		bool running       = _camera.isRunning();
		Camera::Format fmt = _camera.format();
		_status->setText(QString("Open: ") + QString(open ? "True" : "False") +
		                 QString("\n") + QString("Streaming: ") +
		                 QString(running ? "On" : "Off") + QString("\n") +
		                 QString("Format: ") + QString::fromStdString(std::to_string(fmt)));
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::getStatus Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
StreamControls::onConfigure() {
	QSize size         = _resolutionBox->currentData().toSize();
	Camera::Format fmt = {{static_cast<std::uint32_t>(size.width()),
	                       static_cast<std::uint32_t>(size.height())},
	                      static_cast<std::uint8_t>(_fpsBox->value())};
	try {
		_camera.setFormat(fmt);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::setFormat Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
StreamControls::setupGUI() {
	setObjectName(QString::fromUtf8("StreamControls"));
	setLayout(new QVBoxLayout(this));

	// Resolution Field
	{
		QHBoxLayout* resolutionLayout = new QHBoxLayout();
		dynamic_cast<QVBoxLayout*>(layout())->addLayout(resolutionLayout);

		QLabel* resolutionLabel = new QLabel();
		resolutionLabel->setText("Resolution: ");
		resolutionLayout->addWidget(resolutionLabel);

		_resolutionBox = new QComboBox();
		_resolutionBox->addItem("320*240", QSize(320, 240));
		_resolutionBox->addItem("640*480", QSize(640, 480));
		_resolutionBox->addItem("1280*960", QSize(1280, 960));
		_resolutionBox->addItem("2592*1944", QSize(2592, 1944));
		_resolutionBox->setMaxCount(_resolutionBox->count());
		_resolutionBox->setCurrentIndex(1); // 640x480
		resolutionLayout->addWidget(_resolutionBox);
	}
	// Frame Rate field
	{
		QHBoxLayout* frameRateLayout = new QHBoxLayout();
		dynamic_cast<QVBoxLayout*>(layout())->addLayout(frameRateLayout);

		QLabel* requestedFrameRate = new QLabel();
		requestedFrameRate->setText("FrameRate: ");
		frameRateLayout->addWidget(requestedFrameRate);

		_fpsBox = new QSpinBox();
		_fpsBox->setMinimum(5);
		_fpsBox->setMaximum(30);
		_fpsBox->setValue(15);
		frameRateLayout->addWidget(_fpsBox);
	}
	// Status Info
	{
		QLabel* status = new QLabel("Current Format: ");
		layout()->addWidget(status);
		_status = new QLabel("");
		layout()->addWidget(_status);
	}
	QSpacerItem* spacer =
	  new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	layout()->addItem(spacer);
	// Button line
	{
		QHBoxLayout* buttonLayout = new QHBoxLayout();
		dynamic_cast<QVBoxLayout*>(layout())->addLayout(buttonLayout);

		QPushButton* refreshButton = new QPushButton();
		refreshButton->setText("refresh");
		connect(refreshButton, &QPushButton::clicked, this, &StreamControls::onRefresh);
		buttonLayout->addWidget(refreshButton);

		QPushButton* configureButton = new QPushButton();
		configureButton->setText("configure");
		connect(configureButton, &QPushButton::clicked, this, &StreamControls::onConfigure);
		buttonLayout->addWidget(configureButton);
	}
}
