//! @file

#include "CameraGUI.hpp"
#include <QApplication>
#include <Tools.hpp>
#include <iostream>

std::string g_device = "/dev/video0";

//! @brief Display short help of the program.
//! @param[in] name Name of the program (i.e. argv[0]).
static inline void
getUsage(const std::string& name) {
	std::cout << "usage: " << name << " -d /dev/video0" << std::endl
	          << "-d, --device <dev>: device name" << std::endl
	          << std::endl;
}

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int
main(int argc, char** argv) {
	QApplication app(argc, argv);

	// Get Device
	if (isCmdOptionExists(argc, argv, "-d")) {
		g_device = getCmdOption(argc, argv, "-d");
	} else if (isCmdOptionExists(argc, argv, "--device")) {
		g_device = getCmdOption(argc, argv, "--device");
	}

	CameraGUI* gui = new CameraGUI(g_device);
	gui->setMinimumSize(gui->sizeHint());
	gui->show();

	app.exec();
	return 0;
}
