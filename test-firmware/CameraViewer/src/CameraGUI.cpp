//! @file
#include "CameraGUI.hpp"

#include <QApplication>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QShortcut>
#include <QSplitter>
#include <QVBoxLayout>
#include <iostream>

CameraGUI::CameraGUI(const std::string& device, QWidget* parent)
  : QWidget(parent)
  , _camera(device, {{640, 480}, 30}, false)
  , _cameraViewer(0)
  , _cameraControls(0) {
	setupGUI();
}

void
CameraGUI::onOpenCamera() {
	try {
		_camera.openDevice();
		_camera.init();
		_cameraControls->onRefresh();

		_openButton->setEnabled(false);
		_configureButton->setEnabled(true);
		_startButton->setEnabled(false);
		_stopButton->setEnabled(false);
		_closeButton->setEnabled(true);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::openDevice Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
CameraGUI::onConfigureCamera() {
	try {
		_cameraControls->onConfigure();
		_cameraControls->onRefresh();

		_openButton->setEnabled(false);
		_configureButton->setEnabled(true);
		_startButton->setEnabled(true);
		_stopButton->setEnabled(false);
		_closeButton->setEnabled(true);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::initDevice Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
CameraGUI::onStartCamera() {
	try {
		_camera.start();
		_cameraViewer->onStart();
		_cameraControls->onRefresh();

		_openButton->setEnabled(false);
		_configureButton->setEnabled(false);
		_startButton->setEnabled(false);
		_stopButton->setEnabled(true);
		_closeButton->setEnabled(false);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::start Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
CameraGUI::onStopCamera() {
	try {
		_cameraViewer->onStop();
		_camera.stop();
		_cameraControls->onRefresh();

		_openButton->setEnabled(false);
		_configureButton->setEnabled(true);
		_startButton->setEnabled(true);
		_stopButton->setEnabled(false);
		_closeButton->setEnabled(true);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::stop Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
CameraGUI::onCloseCamera() {
	try {
		_camera.closeDevice();
		_cameraControls->onRefresh();

		_openButton->setEnabled(true);
		_configureButton->setEnabled(false);
		_startButton->setEnabled(false);
		_stopButton->setEnabled(false);
		_closeButton->setEnabled(false);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::closeDevice Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
CameraGUI::slotQuit(void) {
	_cameraViewer->onStop();
	qApp->quit();
}

void
CameraGUI::setupGUI() {
	setObjectName(QString::fromUtf8("CameraGUI"));
	setLayout(new QVBoxLayout(this));
	// setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

	// Device name
	{
		QLabel* dev = new QLabel();
		dev->setText(QString("Device: ") + QString::fromStdString(_camera.device()));
		layout()->addWidget(dev);
	}

	// Splitter [Viewer, Controls]
	{
		QSplitter* splitter = new QSplitter();
		splitter->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

		_cameraViewer = new CameraViewer(_camera);
		splitter->addWidget(_cameraViewer);

		_cameraControls = new CameraControls(_camera);
		splitter->addWidget(_cameraControls);

		layout()->addWidget(splitter);
	}
	// Stream State Button
	{
		QHBoxLayout* streamLayout = new QHBoxLayout();

		_openButton = new QPushButton();
		_openButton->setText("Open");
		connect(_openButton, &QPushButton::clicked, this, &CameraGUI::onOpenCamera);
		streamLayout->addWidget(_openButton);

		_configureButton = new QPushButton();
		_configureButton->setText("Configure");
		connect(
		  _configureButton, &QPushButton::clicked, this, &CameraGUI::onConfigureCamera);
		streamLayout->addWidget(_configureButton);

		_startButton = new QPushButton();
		_startButton->setText("Start");
		connect(_startButton, &QPushButton::clicked, this, &CameraGUI::onStartCamera);
		streamLayout->addWidget(_startButton);

		_stopButton = new QPushButton();
		_stopButton->setText("Stop");
		connect(_stopButton, &QPushButton::clicked, this, &CameraGUI::onStopCamera);
		streamLayout->addWidget(_stopButton);

		_closeButton = new QPushButton();
		_closeButton->setText("Close");
		connect(_closeButton, &QPushButton::clicked, this, &CameraGUI::onCloseCamera);
		streamLayout->addWidget(_closeButton);

		dynamic_cast<QBoxLayout*>(layout())->addLayout(streamLayout);
	}

	new QShortcut(Qt::CTRL + Qt::Key_Q, this, SLOT(slotQuit()));

	onCloseCamera();
}
