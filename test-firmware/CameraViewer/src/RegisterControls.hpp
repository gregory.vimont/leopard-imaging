//! @file
#pragma once

#include <Camera.hpp>
#include <QWidget>

class QPushButton;
class RegisterBox;

//! @brief Provides Camera register access view.
class RegisterControls : public QWidget {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	RegisterControls(const RegisterControls&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	RegisterControls& operator=(const RegisterControls&) = delete;
	//! @brief Default Constructor.
	//! @param[in] camera Camera object to bind.
	//! @param[in] parent Parent Widget if any.
	RegisterControls(Camera& camera, QWidget* parent = 0);

	virtual ~RegisterControls() = default;

	public slots:
	//! @brief Updates view from Camera register.
	void onReadRegister();
	//! @brief Updates Camera register.
	void onWriteRegister();

	protected:
	//! @brief Stores the register address requested.
	RegisterBox* _regAddress;
	//! @brief Stores the current value of the register.
	RegisterBox* _regValue;
	//! @brief Stores read button widget.
	QPushButton* _regReadButton;
	//! @brief Stores write button widget.
	QPushButton* _regWriteButton;

	private:
	//! @brief Stores the Camera instance.
	Camera& _camera;

	private:
	//! @brief Initializes the widget.
	void setupGUI();
};
