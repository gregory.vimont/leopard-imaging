//! @file
#pragma once

#include <Camera.hpp>
#include <QComboBox>
#include <QLabel>
#include <QListView>
#include <QPushButton>
#include <QSpinBox>
#include <QStringListModel>
#include <QWidget>

class ExposureMeteringControls;
class RegisterControls;
class StreamControls;

//! @brief Manage Camera Hardware Format.
class CameraControls : public QWidget {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	CameraControls(const CameraControls&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	CameraControls& operator=(const CameraControls&) = delete;
	//! @brief Default Constructor.
	//! @param[in] camera Camera object to bind.
	//! @param[in] parent Parent Widget if any.
	CameraControls(Camera& camera, QWidget* parent = 0);

	//! @brief Destructor.
	virtual ~CameraControls() = default;

	public slots:
	//! @brief Updates the view from Camera state.
	void onRefresh();
	//! @brief Updates StreamControls widget.
	void onConfigure();

	protected:
	//! @brief Stores the Camera instance.
	Camera& _camera;
	//! @brief Stores the StreamControls view.
	StreamControls* _streamControls;
	//! @brief Stores the RegisterControls view.
	RegisterControls* _registerControls;
	//! @brief Stores the ExposureMeteringControls view.
	ExposureMeteringControls* _exposureMeteringControls;

	private:
	//! @brief Initializes the widget.
	void setupGUI();
};
