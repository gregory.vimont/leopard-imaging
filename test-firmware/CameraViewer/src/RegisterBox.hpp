//! @file
#pragma once

#include <Camera.hpp>
#include <QWidget>

class QLabel;
class QPushButton;
class QSpinBox;

//! @brief Display a value in Hex and Decimal.
//! @details Keeps consistency between two QSpinBox.
class RegisterBox : public QWidget {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	RegisterBox(const RegisterBox&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	RegisterBox& operator=(const RegisterBox&) = delete;
	//! @brief Default Constructor.
	//! @param[in] title Name of the register.
	//! @param[in] parent Parent Widget if any.
	RegisterBox(QString title, QWidget* parent = 0);

	virtual ~RegisterBox() = default;

	//! @brief Gets current value of the Box.
	//! @return the current value.
	int value() const;

	public slots:
	//! @brief Updates the value of the Box.
	//! @param[in] value The new value requested.
	void setValue(int value);

	protected:
	//! @brief Stores the Box name to display.
	QLabel* _title;
	//! @brief Display the value in hexadecimal.
	QSpinBox* _HexValue;
	//! @brief Display the value in decimal.
	QSpinBox* _DecValue;

	private:
	//! @brief Initializes the widget.
	//! @param[in] title Label message to display.
	void setupGUI(QString title);
};
