//! @file
#pragma once

#include <Camera.hpp>
#include <QWidget>

class QComboBox;
class QLabel;
class QSpinBox;

//! @brief Widget used to manage camera format.
class StreamControls : public QWidget {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	StreamControls(const StreamControls&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	StreamControls& operator=(const StreamControls&) = delete;
	//! @brief Default Constructor.
	//! @param[in] camera Camera object to bind.
	//! @param[in] parent Parent Widget if any.
	StreamControls(Camera& camera, QWidget* parent = 0);

	virtual ~StreamControls() = default;

	public slots:
	//! @brief Updates view from Camera format.
	void onRefresh();
	//! @brief Updates Camera format.
	void onConfigure();

	protected:
	//! @brief Stores the resolution requested.
	QComboBox* _resolutionBox;
	//! @brief Stores the frame rate requested.
	QSpinBox* _fpsBox;
	//! @brief Displays the Camera active format.
	QLabel* _status;

	private:
	//! @brief Stores the Camera instance.
	Camera& _camera;

	//! @brief Initializes the widget.
	void setupGUI();
};
