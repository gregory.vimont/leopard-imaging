# Camera LI-OV4689 Test Plan
[TOC]

# Introduction {#LIOV4689_Intro}
This test plan is for Stereo Camera LI-OV4689.

All automatic test are divided in several "main" categories:
* [Auto Exposure Controls](#LIOV4689_AEC) test.
* [Generic Controls](#LIOV4689_CONTROLS) test.
* [Format and Streaming](#LIOV4689_FORMATS) test.
* [Version](#LIOV4689_VERSION) test.
* Robustness test.

## Binaries {#LIOV4689_Intro_bin}
To run the test plan you need to run the binary "CameraLIOV4689_UT".

If you are using pure CMake build, binary should be located in @b \<builddir\>/bin/.
If you are using qibuild, binary should be located in @b \<builddir\>/sdk/bin/.

## Testing {#LIOV4689_Intro_test}
To get some help
```sh
CameraLIOV4689_UT -h
```

To run all test:
```sh
CameraLIOV4689_UT -d /dev/video0
```

## Options {#LIOV4689_Intro_misc}
To write image (in PPM P6 format) generated during test plan you can use **--dump** option.
To get more verbosity, you can use **--verbose** or **-v** option

# AEC Test Plan {#LIOV4689_AEC}
## Description {#LIOV4689_AEC_Intro}

Currently on some robots:
- AEC take too much time (more than 15 frames) to converge.
To compute the Illuminance, I compute the means of Y component on all pixel
(without using any weight !)
- AEC seems to be forced at startup whatever the state of ```exposure_auto```
 controls.

## Testing Convergence {#LIOV4689_AEC_Convergence}

```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest-filter="AEC.*Convergence"
```
Here the sequence diagram.
![Convergence](doc/LIOV4689_AEC_Convergence.png)
@image latex doc/LIOV4689_AEC_Convergence.png

## Testing Manual Exposure {#LIOV4689_AEC_Manual}

```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest-filter="AEC.*Manual*"
```

Here the sequence diagram.
![Manual](doc/LIOV4689_AEC_Manual.png)
@image latex doc/LIOV4689_AEC_Manual.png

# Controls Test Plan {#LIOV4689_CONTROLS}
## Description {#LIOV4689_CONTROLS_Intro}
Verify Basic Controls which are:
Brightness, Contrast, Hue, Saturation, Horizontal Flip and Vertical Flip...

You can test on robot head with:
```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Controls*"
```

## Check Tests {#LIOV4689_CONTROLS_Check}
Check tests will verify controls info.

```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Controls.Check*"
```

Here the sequence diagram.
![check](doc/LIOV4689_Controls_Check.png)
@image latex doc/LIOV4689_Controls_Check.png

## Stability Tests {#LIOV4689_CONTROLS_Test}
Will test if cantrol can be changed whatever the camera state is and if state
change don't modify value.

```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Controls.Test*"
```

Here the sequence diagram.
![test](doc/LIOV4689_Controls_Tests.png)
@image latex doc/LIOV4689_Controls_Tests.png

# Format Test Plan {#LIOV4689_FORMATS}
## Description {#LIOV4689_FORMATS_Intro}
Verify Streaming and Format change functionalities.

## Testing {#LIOV4689_FORMATS_Test}
You can test using the pattern.
```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Format*"
```
Here the sequence diagram.

![test_plan](doc/LIOV4689_Formats_Periodicity.png)
@image latex doc/LIOV4689_Formats_Periodicity.png

Here the sequence diagram.

![test_plan](doc/LIOV4689_Formats_Change.png)
@image latex doc/LIOV4689_Formats_Change.png

# Version Test Plan {#LIOV4689_VERSION}
## Description {#LIOV4689_VERSION_Intro}
Retrieve the firmware version.

## Testing {#LIOV4689_VERSION_Test}
Verify if the firmware loaded is the last one.
```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Version*"
```

# Robustness Test Plan {#LIOV4689_VERSION}
## Description {#LIOV4689_VERSION_Intro}
Do many open/configure/getImage/close sequence

## Testing {#LIOV4689_VERSION_Test}
To launch the test.
```sh
./CameraLIOV4689_UT -d /dev/video-stereo --gtest_filter="Robustness*"
```
Here the activity diagram.

![test_plan](doc/LIOV4689_Robustness.png)
@image latex doc/LIOV4689_Robustness.png
