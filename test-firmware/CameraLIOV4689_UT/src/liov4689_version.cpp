//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV4689.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;

//! @test Checks CameraLIOV4689 firmware revision.
TEST(Version, CheckRevision) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Get Version");
	std::uint32_t version;
	ASSERT_NO_THROW(version = cam.getVersion());
	std::stringstream ss;
	ss << cam.device() << ": Version: 0x" << std::hex << version;
	SCOPED_TRACE(ss.str());
	EXPECT_EQ(0x2305u, version);
}
