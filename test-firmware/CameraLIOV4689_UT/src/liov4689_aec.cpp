//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV4689.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;

//! @test Checks CameraLIOV4689 convergence take up to 15 frames when AEC enabled.
TEST(AEC, TestConvergence) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	ASSERT_NO_THROW(cam.flushBuffers());

	SCOPED_TRACE(cam.device() + ": Get 40 Frames");
	std::vector<std::unique_ptr<Camera::Image>> images;
	for (int j = 0; j < 40; ++j) {
		std::unique_ptr<Camera::Image> img;
		EXPECT_NO_THROW(img = cam.getImage());
		if (img) {
			SCOPED_TRACE(cam.device() + ": Means: " +
			             std::to_string(CameraLIOV4689::computeImageMean(*img)));
			images.push_back(std::move(img));
		}
	}
	if (g_enableVerbose) {
		for (auto const& it : images) {
			std::cout << "(VERBOSE) Frame: " << it->seq << " ts: " << it->seconds << "s,"
			          << it->milliseconds << "ms"
			          << " Means: " << CameraLIOV4689::computeImageMean(*it.get())
			          << std::endl;
		}
	}
	// Compute Reference
	ASSERT_TRUE(images.size() >= 10);
	double meanReference = 0.0;
	for (auto it = images.end() - 10; it != images.end(); ++it) {
		meanReference += CameraLIOV4689::computeImageMean(**it);
	}
	meanReference = meanReference / 10.0;
	if (g_enableVerbose) {
		std::cout << "(VERBOSE) Reference Means based on last ten Frames: "
		          << std::to_string(meanReference);
	}
	SCOPED_TRACE(cam.device() + "Reference Means based on last ten Frames: " +
	             std::to_string(meanReference));

	// Start at 15th Buffer AEC must have converged.
	for (const auto& img : images) {
		if (img->seq > 15) {
			double frameMeanReference = CameraLIOV4689::computeImageMean(*img.get());
			SCOPED_TRACE(cam.device() + ": Frame " + std::to_string(img->seq) +
			             " Means: " + std::to_string(frameMeanReference));
			EXPECT_TRUE((meanReference + 2) > frameMeanReference);
			EXPECT_TRUE((meanReference - 2) < frameMeanReference);
		}
	}

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Close Device");
	ASSERT_NO_THROW(cam.closeDevice());
}
