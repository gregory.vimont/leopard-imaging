//! @file
#include <gtest/gtest.h>
#include <thread>

#include "tools.hpp"
#include <CameraLIOV4689.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;

//! @test Checks CameraLIOV4689 Check camera robustness by doing start/getImage/stop loops.
TEST(Robustness, getImage) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
    int loop = 100;
    int frame_max = 15;
    int sleepTime = 5000; // Sleep during sleepTime between each loop (ms)
    int failCount = 0;    // Count failing loop

    for (int i = 0; i < loop; ++i) {
        int errCount = 0;
        setupCameraLIOV4689(cam, g_preference);
        ASSERT_NO_THROW(cam.start());
        try {
            cam.flushBuffers();
        }
        catch (std::exception& e) {
            failCount++;
            ADD_FAILURE() << "Exception during flushBuffers";
            ASSERT_NO_THROW(cam.stop());
            continue;
        }

        for (int j = 0; j < frame_max; ++j) {
            std::unique_ptr<Camera::Image> img;
            try {
                img = cam.getImage();
            }
            catch (std::exception& e) {
                errCount++;
                ADD_FAILURE() << "Exception during getImage";
            }
        }

        ASSERT_NO_THROW(cam.stop());
        ASSERT_NO_THROW(cam.closeDevice());

        if (errCount > 0)
            failCount++;

        std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
    }

    if (failCount > 0) {
        ADD_FAILURE() << failCount << " loop(s) failed out of " << loop;
    }

}
