//! @file
#pragma once
#include <gtest/gtest.h>

#include <Camera.hpp>

//! @brief Maximum number of Image to dump on error.
#define DEFECT_NB 5
//! @brief Number of Image to grabe when testing robustness.
#define IMG_LOOP 250

//! @brief Helper to Open, Init and SetFormat.
//! @param[in] cam The camera instance to use.
//! @param[in] format The format requested to setup.
void setupCameraLIOV4689(Camera& cam, const Camera::Format& format);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance to use.
//! @param[in] format The format requested to setup.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV4689Stream(const std::string& testName,
                              Camera& cam,
                              const Camera::Format& format,
                              bool enableDrop);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance to use.
//! @param[in] prevFormat The previous format used.
//! @param[in] currFormat The current format in used.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV4689Stream(const std::string& testName,
                              Camera& cam,
                              const Camera::Format& prevFormat,
                              const Camera::Format& currFormat,
                              bool enableDrop);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance used.
//! @param[in] format The format requested to setup.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV4689PatternStream(const std::string& testName,
                                     Camera& cam,
                                     const Camera::Format& format,
                                     bool enableDrop);
