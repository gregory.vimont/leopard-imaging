//! @file
#pragma once

#include "Camera.hpp"
#include <vector>

struct v4l2_buffer;

//! @brief Interface for a V4L2 Camera object.
class CameraV4L2 : public Camera {
	public:
	//! @brief Default constructor.
	//! @param[in] device Device file to use.
	//! @param[in] format Default format to use at init.
	//! @param[in] verbose Enable verbose log.
	CameraV4L2(std::string device, Format format, bool verbose);
	~CameraV4L2() override;

	void openDevice() override;
	void closeDevice() override;
	bool isOpen() const override;

	std::uint32_t getVersion() const override;

	ParameterInfo getParameterInfo(std::uint32_t param) const override;
	std::int32_t getParameter(std::uint32_t param) const override;
	void setParameter(std::uint32_t param, std::int32_t value) override;

	ParameterInfo getExtUnitInfo(std::uint8_t reg) const override;
	std::int16_t getExtUnit(std::uint8_t reg) const override;
	void setExtUnit(std::uint8_t reg, std::int16_t value) override;

	void init() override;
	void start() override;
	void stop() override;
	bool isRunning() const override;

	std::unique_ptr<Camera::Image> flushBuffers() override;
	std::unique_ptr<Image> getImage() override;

	protected:
	void _updateDeviceResolution(bool bypassCheck) override;
	void _updateDeviceFrameRate(bool bypassCheck) override;
	void _updateRingBufferSize() override;

	//! @brief Initializes V4L2 ring buffer and Mmap them.
	void initMmapStreaming();
	//! @brief Initializes V4L2 ring buffer and Mmap them.
	//! @param[in] bufferCount Update the size of the ring Buffer.
	void initMmapStreaming(int bufferCount);
	//! @brief Unmmaps all buffer and remove them.
	void unInitMmapStreaming();

	//! @brief Starts V4L2 device grabbing.
	void startStreaming();
	//! @brief Stops V4L2 device grabbing.
	void stopStreaming();

	//! @brief Gets a Image from the V4L2 ring buffer.
	//! @return the mmapped V4L2 kernel buffer.
	v4l2_buffer lockKernelBuffer();
	//! @brief Releases Image belongs to v4l2 ring buffer.
	//! @param[in] buffer The mmapped V4L2 kernel buffer to release.
	void releaseKernelBuffer(v4l2_buffer& buffer);
	//! @brief Dump a v4l2 buffer if stdout is not a tty.
	//! @param[in] buffer The mmapped V4L2 kernel buffer to print.
	void printBuffer(v4l2_buffer& buffer) const;

	//! @brief Stores the file descriptor.
	std::int32_t _fd;
	//! @brief Stores the list of mmap V4L2 kernel buffer.
	std::vector<std::pair<void*, std::uint32_t>> _v4l2Buffers;
	//! @brief Stores if stream is running.
	bool _running;
};
