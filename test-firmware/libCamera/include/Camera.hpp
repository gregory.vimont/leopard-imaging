//! @file
#pragma once

#include <memory>
#include <ostream>
#include <sstream>
extern "C" {
#include <linux/videodev2.h> // V4L2 Controls
}

//! @brief Interface for a camera object.
class Camera {
	public:
	struct Format;
	//! @brief Default constructor.
	//! @param[in] device Device file to use.
	//! @param[in] format Default format to use at init.
	//! @param[in] verbose Enable verbose log.
	Camera(std::string device, Format format, bool verbose);
	virtual ~Camera() = default;

	//! @brief Gets the device file in use.
	//! @return the Device file in use.
	inline std::string device() const noexcept { return _device; }
	//! @brief Opens the device file.
	//! @throw std::runtime_error file can't be open.
	virtual void openDevice() = 0;
	//! @brief Closes the device file.
	virtual void closeDevice() = 0;
	//! @brief Checks if the device file is open.
	//! @return true if device file is open, false otherwise.
	virtual bool isOpen() const = 0;

	//! @brief Gets the version of the camera driver or firmware.
	//! @return the version of the firmware.
	virtual std::uint32_t getVersion() const = 0;

	//! @brief Stores a Camera format object.
	struct Format {
		//! @brief Resolution object.
		struct Resolution {
			//! @brief Stores the width.
			std::uint32_t width;
			//! @brief Stores the height.
			std::uint32_t height;
			//! @brief Checks if two Resolution objects are equivalent.
			//! @param[in] rhs Resolution object to compare with.
			//! @return true if equivalent, false otherwise.
			inline bool operator==(const Resolution& rhs) const {
				return width == rhs.width && height == rhs.height;
			}
			//! @brief Injects resolution state in the output stream.
			//! @param[in,out] os Output stream to fill.
			//! @param[in] resolution Resolution object to log.
			//! @return the output stream.
			inline friend std::ostream& operator<<(std::ostream& os,
			                                       const Resolution& resolution) {
				return os << "{width: " << std::to_string(resolution.width)
				          << ", height: " << std::to_string(resolution.height) << "}";
			}
		}
		//! @brief Stores the Resolution.
		resolution;
		//! @brief Stores the frame rate.
		std::uint8_t frameRate;
		//! @brief Checks if two Format objects are equivalent.
		//! @param[in] rhs Format object to compare with.
		//! @return true if equivalent, false otherwise.
		inline bool operator==(const Format& rhs) const {
			return resolution == rhs.resolution && frameRate == rhs.frameRate;
		}
		//! @brief Injects format state in the output stream.
		//! @param[in,out] os Output stream to fill.
		//! @param[in] format Format object to log.
		//! @return the output stream.
		friend inline std::ostream& operator<<(std::ostream& os, const Format& format) {
			return os << "{resolution: " << format.resolution
			          << ", frame_rate: " << std::to_string(format.frameRate) << "}";
		}
	};

	//! @brief Updates the Format of the Camera.
	//! @details Sets the Frame Rate, then the Resolution of the Camera.
	//! @param[in] format New Format requested.
	//! @param[in] bypassCheck Checks if format is equivalent before update.
	void setFormat(const Camera::Format& format, bool bypassCheck = false);
	//! @brief Gets the current Camera Format.
	//! @return the current Format in use.
	inline Format format() const noexcept { return _format; }
	//! @brief Gets the expected size of an Image.
	//! @return the size of an image in byte.
	inline std::uint32_t bufferSize() const noexcept {
		return _format.resolution.width * _format.resolution.height * 2u;
	}

	//! @brief Updates the number of internal V4L2 kernel buffer.
	//! @param[in] bufferNumber Number of buffer requested.
	void setRingBufferSize(std::uint32_t bufferNumber);
	//! @brief Gets the current number of V4L2 kernel buffer.
	//! @return the number of V4L2 kernel buffer.
	inline std::uint32_t ringBufferSize() const noexcept { return _ringBufferSize; }

	//! @brief Stores information about a Camera hardware parameter.
	struct ParameterInfo {
		ParameterInfo()  = default;
		~ParameterInfo() = default;
		//! @brief Creates a ParameterInfo instance with all value set.
		//! @param[in] pmin Minimum value allowed.
		//! @param[in] pmax Maximum value allowed.
		//! @param[in] pstep Step between two consecutive values.
		//! @param[in] pdefault Default value when resetting.
		ParameterInfo(int pmin, int pmax, int pstep, int pdefault)
		  : min(pmin)
		  , max(pmax)
		  , step(pstep)
		  , def(pdefault) {}

		int min;  //!< @brief minimum value.
		int max;  //!< @brief maximum value.
		int step; //!< @brief step between two consecutive values.
		int def;  //!< @brief default value when resetting camera.
	};
	//! @brief Gets parameter info.
	//! @param[in] param Parameter requested.
	//! @return information associated to this Parameter.
	virtual ParameterInfo getParameterInfo(std::uint32_t param) const = 0;
	//! @brief Gets the current value of a Parameter.
	//! @param[in] param Parameter requested.
	//! @return current value of the Parameter.
	virtual std::int32_t getParameter(std::uint32_t param) const = 0;
	//! @brief Updates the current value of a Parameter.
	//! @param[in] param Parameter requested.
	//! @param[in] value New value requested.
	virtual void setParameter(std::uint32_t param, std::int32_t value) = 0;

	//! @brief Gets extension unit info.
	//! @param[in] reg Register requested.
	//! @return information associated to this extension unit.
	virtual ParameterInfo getExtUnitInfo(std::uint8_t reg) const = 0;
	//! @brief Gets the current value of a Register.
	//! @param[in] reg Register requested.
	//! @return current value of the Register.
	virtual std::int16_t getExtUnit(std::uint8_t reg) const = 0;
	//! @brief Updates the current value of a Register.
	//! @param[in] reg Register requested.
	//! @param[in] value New value requested.
	virtual void setExtUnit(std::uint8_t reg, std::int16_t value) = 0;

	//! @brief Initializes the Camera.
	virtual void init() = 0;
	//! @brief Starts the Camera stream.
	virtual void start() = 0;
	//! @brief Stops the Camera stream.
	virtual void stop() = 0;
	//! @brief Checks if the Camera is streaming.
	//! @return true if camera is streaming, false otherwise.
	virtual bool isRunning() const = 0;

	//! @brief Stores a Camera image object.
	struct Image {
		//! @brief Default Constructor.
		Image()  = default;
		~Image() = default;

		//! @brief Stores the seconds of the Camera timestamp.
		std::uint32_t seconds;
		//! @brief Stores the milliseconds of the Camera timestamp.
		std::uint32_t milliseconds;
		//! @brief Stores the sequence number of the Image.
		std::uint32_t seq;
		//! @brief Stores the width of the Image.
		std::uint32_t width;
		//! @brief Stores the height of the Image.
		std::uint32_t height;
		//! @brief Stores the Image buffer.
		std::unique_ptr<std::uint8_t[]> buffer;
	};

	//! @brief Flushs first empty buffers.
	//! @return the first correct Image.
	virtual std::unique_ptr<Image> flushBuffers() = 0;
	//! @brief Gets Image.
	//! @return the next available Image.
	virtual std::unique_ptr<Image> getImage() = 0;

	//! @brief Writes an Image to a file using PPM format.
	//! @param[in] image Image to write.
	//! @param[in] filename Filename requested.
	static void writeImage2PPM(const Image& image, const std::string& filename);
	//! @brief Computes the Image Y component Mean of all pixels.
	//! @param[in] image Image to analyze.
	//! @return Mean value of Y component in the Image.
	static double computeImageMean(const Image& image);

	protected:
	//! @brief Updates Device Resolution.
	//! @param[in] bypassCheck If true don't check previous value and update anyway.
	virtual void _updateDeviceResolution(bool bypassCheck) = 0;
	//! @brief Updates Device Frame Rate.
	//! @param[in] bypassCheck If true don't check previous value and update anyway.
	virtual void _updateDeviceFrameRate(bool bypassCheck) = 0;
	//! @brief Updates Device V4L2 buffer number.
	virtual void _updateRingBufferSize() = 0;

	//! @brief Name of the device file.
	const std::string _device;
	//! @brief Stores the format requested.
	Format _format;
	//! @brief Stores the buffer number requested.
	std::uint32_t _ringBufferSize;
	//! @brief Enable trace log.
	bool _enableVerbose;

	//! @brief Injects camera state in the output stream.
	//! @param[in,out] os Output stream to fill.
	//! @param[in] camera Camera object to log.
	//! @return the output stream.
	friend std::ostream& operator<<(std::ostream& os, const Camera& camera);
};

namespace std {
//! @brief Overload of std::to_string for Format object.
//! @param[in] format Format instance to stringify.
//! @return a std::string with the format information.
inline string
to_string(const Camera::Format& format) {
	std::stringstream ss;
	ss << format;
	return ss.str();
}
//! @brief Overload of std::to_string for Camera object.
//! @param[in] camera Camera instance to stringify.
//! @return a std::string with the camera information.
inline string
to_string(const Camera& camera) {
	std::stringstream ss;
	ss << camera;
	return ss.str();
}
} // namespace std
