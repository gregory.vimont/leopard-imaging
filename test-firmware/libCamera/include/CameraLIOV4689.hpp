//! @file
#pragma once

#include "CameraV4L2.hpp"

//! @brief Specialization for the Stereo Camera.
class CameraLIOV4689 : public CameraV4L2 {
	public:
	//! @brief Default constructor.
	//! @param[in] device Device file to use.
	//! @param[in] format Default format to use at init.
	//! @param[in] verbose Enable verbose log.
	CameraLIOV4689(std::string device, Format format, bool verbose)
	  : CameraV4L2(std::move(device), std::move(format), std::move(verbose)) {}
	virtual ~CameraLIOV4689() = default;

	std::uint32_t getVersion() const override;
	void init() override;

	//! @brief Enum used to list OV580 sub device (i.e. image sensor).
	enum class Sensor { SCCB0, SCCB1 };
	//! @brief Injects sensor state in the output stream.
	//! @param[in,out] os Output stream to fill.
	//! @param[in] sensor Sensor object to log.
	//! @return the output stream.
	friend std::ostream& operator<<(std::ostream& os, CameraLIOV4689::Sensor sensor);

	//! @brief Gets the current value of a sensor Register.
	//! @param[in] sensor Index of the sensor requested.
	//! @param[in] address Register address requested.
	//! @return current value at the requested address.
	std::uint8_t readSensorRegister(Sensor sensor, std::uint16_t address) const;
	//! @brief Updates the current value of a sensor Register.
	//! @param[in] sensor Index of the sensor requested.
	//! @param[in] address Register address requested.
	//! @param[in] value New value requested.
	void writeSensorRegister(Sensor sensor, std::uint16_t address, std::uint8_t value);

	//! @brief Gets the current value of an OV580 register address.
	//! @param[in] address Register address requested.
	//! @return current value at the requested address.
	std::uint8_t readISPRegister(uint32_t address) const;
	//! @brief Updates the current value of an OV580 register address.
	//! @param[in] address Register address requested.
	//! @param[in] value New value requested.
	void writeISPRegister(std::uint32_t address, uint8_t value);
};
