//! @file
#pragma once

#include <algorithm>
#include <string>
#include <vector>

//! @brief Gets the value of a command option.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @param[in] option The command option to search.
//! @return the value of the command option if exists, empty string otherwise.
inline std::string
getCmdOption(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	std::vector<std::string>::const_iterator itr =
	  std::find(args.begin(), args.end(), option);
	if (itr != args.end() && ++itr != args.end()) {
		return *itr;
	}
	return std::string();
}

//! @brief Checks if a command option exists.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @param[in] option The command option to search.
//! @return true if command option exists, false otherwise.
inline bool
isCmdOptionExists(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	return std::find(args.begin(), args.end(), option) != args.end();
}

//! @brief Gets the index of a command option.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @param[in] option The command option to search.
//! @return index of the command option in the argv container.
inline std::int32_t
getCmdIndex(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	for (std::int32_t idx = 0; idx < argc; ++idx) {
		if (args[idx] == option) return idx;
	}
	return -1;
}
