//! @file
#pragma once

#include "CameraV4L2.hpp"
#include <array>

//! @brief Specialization for an UVC OV5640 Camera object.
class CameraLIOV5640 : public CameraV4L2 {
	public:
	//! @brief Default constructor.
	//! @param[in] device Device file to use.
	//! @param[in] format Default format to use at init.
	//! @param[in] verbose Enable verbose log.
	CameraLIOV5640(std::string device, Format format, bool verbose)
	  : CameraV4L2(std::move(device), std::move(format), std::move(verbose)) {}
	virtual ~CameraLIOV5640() = default;

	//! @brief Control Extension Units.
	enum ExtensionUnit : int {
		VERSION           = 0x03,
		AVERAGE_LUMINANCE = 0x08,
		TEST_PATTERN      = 0x0a,
		HORIZONTAL_FLIP   = 0x0c,
		VERTICAL_FLIP     = 0x0d
	};

	std::uint32_t getVersion() const override;
	void init() override;

	//! @brief Gets the current value of the image sensor Register.
	//! @param[in] addr Register address requested.
	//! @return current value at the requested address.
	std::uint16_t readRegister(std::uint16_t addr) const;
	//! @brief Updates the current value of the image sensor Register.
	//! @param[in] addr Register address requested.
	//! @param[in] value New value requested.
	void writeRegister(std::uint16_t addr, std::uint16_t value);

	//! @brief Gets the current values of the image sensor exposure windowing system.
	//! @return current values of the exposure windowing system.
	std::array<std::uint8_t, 17> readExposureMetering() const;
	//! @brief Gets the current values of the image sensor exposure windowing system.
	//! @param[in] values Current values of the exposure windowing system.
	void writeExposureMetering(std::array<std::uint8_t, 17> values);
};
