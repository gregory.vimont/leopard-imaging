//! @file
#include <CameraV4L2.hpp>

extern "C" {
#include <fcntl.h>           // ::open
#include <linux/usb/video.h> // UVC Controls
#include <linux/uvcvideo.h>  // UVC ExtUnit Controls
#include <linux/videodev2.h> // V4L2 Controls
#include <sys/ioctl.h>       // ::ioctl
#include <sys/mman.h>        // ::mmap
#include <unistd.h>          // ::close
}
#include <cassert>
#include <cerrno>
#include <chrono>
#include <cstring> // std::memset
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <thread>

CameraV4L2::CameraV4L2(std::string device, Format format, bool verbose)
  : Camera(std::move(device), std::move(format), std::move(verbose))
  , _fd(-1)
  , _v4l2Buffers()
  , _running(false) {}

CameraV4L2::~CameraV4L2() {
	closeDevice();
}

void
CameraV4L2::openDevice() {
	if (_fd != -1) return; // already open

	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": opening device" << std::endl;
	}
	// fd = ::open(_device.c_str(), O_RDWR | O_CLOEXEC);
	_fd = ::open(_device.c_str(), O_RDWR | O_NONBLOCK | O_CLOEXEC);
	if (_fd < 0) {
		_fd = -1;
		throw std::runtime_error(_device + ": open fails: " + std::strerror(errno));
	}
}

void
CameraV4L2::closeDevice() {
	if (_fd == -1) return; // already close

	// Force stop is running
	stop();

	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": closing device" << std::endl;
	}
	_fd = ::close(_fd);
	if (_fd < 0) {
		_fd = -1;
		throw std::runtime_error(_device + ": close fails: " + std::strerror(errno));
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	_fd = -1;
	//_format = {{0,0},0};
}

bool
CameraV4L2::isOpen() const {
	return (_fd != -1);
}

std::uint32_t
CameraV4L2::getVersion() const {
	throw std::runtime_error("Not supported for generic backend !");
}

Camera::ParameterInfo
CameraV4L2::getParameterInfo(std::uint32_t param) const {
	struct v4l2_queryctrl queryctrl;
	std::memset(&queryctrl, 0, sizeof(queryctrl));
	queryctrl.id = param;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": QUERYCTRL id:" << std::hex
		          << queryctrl.id << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, VIDIOC_QUERYCTRL, &queryctrl)) {
		std::cerr << "(ERROR) " << _device << ": QUERYCTRL ("
		          << std::to_string(_IOC_NR(VIDIOC_QUERYCTRL))
		          << "): " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": VIDIOC_QUERYCTRL: " + std::strerror(errno));
	}
	return ParameterInfo(
	  queryctrl.minimum, queryctrl.maximum, queryctrl.step, queryctrl.default_value);
}

std::int32_t
CameraV4L2::getParameter(std::uint32_t param) const {
	struct v4l2_control control;
	std::memset(&control, 0, sizeof(control));
	control.id = param;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": G_CTRL id:" << std::hex << control.id
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, VIDIOC_G_CTRL, &control)) {
		std::cerr << "(ERROR) " << _device << ": G_CTRL ("
		          << std::to_string(_IOC_NR(VIDIOC_G_CTRL)) << "): " << std::strerror(errno)
		          << std::endl;
		throw std::runtime_error(_device + ": VIDIOC_G_CTRL: " + std::strerror(errno));
	}
	return control.value;
}

void
CameraV4L2::setParameter(std::uint32_t param, std::int32_t value) {
	struct v4l2_control control;
	std::memset(&control, 0, sizeof(control));
	control.id    = param;
	control.value = value;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": S_CTRL id:" << std::hex << control.id
		          << std::dec << " value:" << control.value << std::endl;
	}
	if (-1 == ::ioctl(_fd, VIDIOC_S_CTRL, &control)) {
		std::cerr << "(ERROR) " << _device << ": G_CTRL ("
		          << std::to_string(_IOC_NR(VIDIOC_S_CTRL)) << "): " << std::strerror(errno)
		          << std::endl;
		throw std::runtime_error(_device + ": VIDIOC_S_CTRL: " + std::strerror(errno));
	}
}

Camera::ParameterInfo
CameraV4L2::getExtUnitInfo(std::uint8_t reg) const {
	std::uint8_t data[2];
	ParameterInfo result(0, 0, 0, 0);
	struct uvc_xu_control_query xu_queryctrl;
	std::memset(&xu_queryctrl, 0, sizeof(xu_queryctrl));
	xu_queryctrl.unit     = 3;
	xu_queryctrl.selector = reg;
	xu_queryctrl.size     = 2;
	xu_queryctrl.data     = data;

	xu_queryctrl.query = UVC_GET_MIN;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_GET_MIN id:" << std::hex << int(reg)
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_MIN fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_MIN fails: " + std::strerror(errno));
	}
	result.min = (data[1] << 8) | data[0];

	xu_queryctrl.query = UVC_GET_MAX;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_GET_MAX id:" << std::hex << int(reg)
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_MAX fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_MAX fails: " + std::strerror(errno));
	}
	result.max = (data[1] << 8) | data[0];

	xu_queryctrl.query = UVC_GET_RES;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_GET_RES id:" << std::hex << int(reg)
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_RES fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_RES fails: " + std::strerror(errno));
	}
	result.step = (data[1] << 8) | data[0];

	xu_queryctrl.query = UVC_GET_DEF;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_GET_DEF id:" << std::hex << int(reg)
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_DEF fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_DEF fails: " + std::strerror(errno));
	}
	result.def = (data[1] << 8) | data[0];

	return result;
}

std::int16_t
CameraV4L2::getExtUnit(std::uint8_t reg) const {
	std::uint8_t data[2];
	struct uvc_xu_control_query queryctrl;
	std::memset(&queryctrl, 0, sizeof(queryctrl));
	queryctrl.unit     = 3;
	queryctrl.selector = reg;
	queryctrl.query    = UVC_GET_CUR;
	queryctrl.size     = 2;
	queryctrl.data     = data;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_GET_CUR id:" << std::hex << int(reg)
		          << std::dec << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_CUR fails: " + std::strerror(errno));
	}
	return (data[1] << 8) | data[0];
}

void
CameraV4L2::setExtUnit(std::uint8_t reg, std::int16_t value) {
	std::uint8_t data[2];
	data[0] = value & 0xff;
	data[1] = (value >> 8) & 0xff;

	struct uvc_xu_control_query queryctrl;
	std::memset(&queryctrl, 0, sizeof(queryctrl));
	queryctrl.unit     = 3;
	queryctrl.selector = reg;
	queryctrl.query    = UVC_SET_CUR;
	queryctrl.size     = 2;
	queryctrl.data     = data;
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": UVC_SET_CUR id:" << std::hex << int(reg)
		          << std::dec << " value:" << value << std::endl;
	}
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
}

void
CameraV4L2::init() {
	openDevice();
	_updateDeviceResolution(false);
	_updateDeviceFrameRate(false);
}

void
CameraV4L2::start() {
	if (_running) return;
	initMmapStreaming();
	startStreaming();
	_running = true;
}

void
CameraV4L2::stop() {
	if (!_running) return;
	_running = false;
	stopStreaming();
	unInitMmapStreaming();
}

bool
CameraV4L2::isRunning() const {
	return _running;
}

std::unique_ptr<Camera::Image>
CameraV4L2::flushBuffers() {
	assert(isRunning());
	v4l2_buffer buffer;
	std::memset(&buffer, 0, sizeof(v4l2_buffer));
	buffer.index      = 0;
	buffer.type       = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buffer.memory     = V4L2_MEMORY_MMAP;
	std::size_t count = 0;

	::ioctl(_fd, VIDIOC_DQBUF, &buffer);
	while (buffer.sequence < 1) {
		if (_enableVerbose) {
			std::cout << "(VERBOSE) " << _device << ": flush (lock & release) buffer, "
			          << " timecode: " << std::setw(3) << int32_t(buffer.sequence) << "seq"
			          << std::endl;
		}
		::ioctl(_fd, VIDIOC_QBUF, &buffer);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000 / format().frameRate));
		::ioctl(_fd, VIDIOC_DQBUF, &buffer);
		// throw after ~10s
		if (count++ > 10 * format().frameRate) {
			std::cerr << "(ERROR) " << _device << ": flush: can't dequeue a single image"
			          << std::endl;
			throw std::runtime_error(_device + ": flush: can't dequeue a single image");
		}
	}
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": flush (only lock) buffer, "
		          << " timecode: " << std::setw(3) << int32_t(buffer.sequence) << "seq"
		          << std::endl;
	}
	std::unique_ptr<Image> img(new Camera::Image);
	img->width  = _format.resolution.width;
	img->height = _format.resolution.height;
	img->buffer.reset(new std::uint8_t[bufferSize()]);
	std::memcpy(img->buffer.get(), _v4l2Buffers[buffer.index].first, bufferSize());
	img->seconds      = buffer.timestamp.tv_sec;
	img->milliseconds = buffer.timestamp.tv_usec / 1000;
	img->seq          = buffer.sequence;
	::ioctl(_fd, VIDIOC_QBUF, &buffer);
	return img;
}

std::unique_ptr<Camera::Image>
CameraV4L2::getImage() {
	assert(isRunning());
	std::unique_ptr<Image> img(new Camera::Image);
	img->width  = _format.resolution.width;
	img->height = _format.resolution.height;
	img->buffer.reset(new std::uint8_t[bufferSize()]);

	v4l2_buffer buffer = lockKernelBuffer();
	std::memcpy(img->buffer.get(), _v4l2Buffers[buffer.index].first, bufferSize());
	img->seconds      = buffer.timestamp.tv_sec;
	img->milliseconds = buffer.timestamp.tv_usec / 1000;
	img->seq          = buffer.sequence;

	releaseKernelBuffer(buffer);
	return img;
}

extern "C" {
//! @brief Small helper to remove warning (old style cast).
//! @return the kernel code for YUV422 format.
static inline std::uint32_t
getYUYVformat() {
	return V4L2_PIX_FMT_YUYV;
}
}

void
CameraV4L2::_updateDeviceResolution(bool bypassCheck) {
	openDevice();
	// Stream must be stop to change resolution
	bool running = isRunning();
	stop();

	// First get Hardware Configuration
	bool currentFormatDifferent = true;
	if (!bypassCheck) {
		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; // for a video device
		if (-1 == ::ioctl(_fd, VIDIOC_G_FMT, &fmt)) {
			std::cerr << "(ERROR) " << _device << ": VIDIOC_G_FMT ("
			          << std::to_string(_IOC_NR(VIDIOC_G_FMT))
			          << "): " << std::strerror(errno) << std::endl;
			throw std::runtime_error(_device + ": VIDIOC_G_FMT: " + std::strerror(errno));
		}
		if (fmt.fmt.pix.width != _format.resolution.width ||
		    fmt.fmt.pix.height != _format.resolution.height ||
		    fmt.fmt.pix.colorspace != V4L2_COLORSPACE_JPEG ||
		    fmt.fmt.pix.pixelformat != getYUYVformat()) {
			currentFormatDifferent = true;
		} else {
			if (_enableVerbose) {
				std::cout << "(VERBOSE) " << _device
				          << ": current resolution identical, nothing to do" << std::endl;
			}
			currentFormatDifferent = false;
		}
	}

	// Second SetFormat
	if (currentFormatDifferent) {
		if (_enableVerbose) {
			std::cout << "(VERBOSE) " << _device << ": set resolution to "
			          << _format.resolution.width << "x" << _format.resolution.height
			          << std::endl;
		}
		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE; // for a video device
		fmt.fmt.pix.field       = V4L2_FIELD_NONE;             // not interlaced camera
		fmt.fmt.pix.width       = _format.resolution.width;
		fmt.fmt.pix.height      = _format.resolution.height;
		fmt.fmt.pix.colorspace  = V4L2_COLORSPACE_JPEG;
		fmt.fmt.pix.pixelformat = getYUYVformat();
		if (-1 == ::ioctl(_fd, VIDIOC_S_FMT, &fmt)) {
			std::cerr << "(ERROR) " << _device << ": VIDIOC_S_FMT ("
			          << std::to_string(_IOC_NR(VIDIOC_S_FMT))
			          << "): " << std::strerror(errno) << std::endl;
			throw std::runtime_error(_device + ": VIDIOC_S_FMT: " + std::strerror(errno));
		}
	}

	if (running) // restart stream if needed
		start();
}

void
CameraV4L2::_updateDeviceFrameRate(bool bypassCheck) {
	openDevice();
	// Stream must be stop to change resolution
	bool running = isRunning();
	stop();

	// First get Hardware Configuration
	bool currentFormatDifferent = true;
	if (!bypassCheck) {
		struct v4l2_streamparm parm;
		std::memset(&parm, 0, sizeof(parm));
		parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == ::ioctl(_fd, VIDIOC_G_PARM, &parm)) {
			std::cerr << "(ERROR) " << _device << ": VIDIOC_G_PARM ("
			          << std::to_string(_IOC_NR(VIDIOC_G_PARM))
			          << "): " << std::strerror(errno) << std::endl;
			throw std::runtime_error(_device + ": VIDIOC_G_PARM: " + std::strerror(errno));
		}
		if (parm.parm.capture.timeperframe.numerator != 1 ||
		    parm.parm.capture.timeperframe.denominator != _format.frameRate) {
			currentFormatDifferent = true;
		} else {
			if (_enableVerbose) {
				std::cout << "(VERBOSE) " << _device
				          << ": current framerate identical, nothing to do" << std::endl;
			}
			currentFormatDifferent = false;
		}
	}

	// Second SetFormat
	if (currentFormatDifferent) {
		if (_enableVerbose) {
			std::cout << "(VERBOSE) " << _device << ": set framerate to "
			          << int(_format.frameRate) << std::endl;
		}
		struct v4l2_streamparm parm;
		std::memset(&parm, 0, sizeof(parm));
		parm.type                                  = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		parm.parm.capture.timeperframe.numerator   = 1;
		parm.parm.capture.timeperframe.denominator = _format.frameRate;
		// indicates that the device can vary its frame rate
		parm.parm.capture.capability   = V4L2_CAP_TIMEPERFRAME;
		parm.parm.capture.extendedmode = 0;
		if (-1 == ::ioctl(_fd, VIDIOC_S_PARM, &parm)) {
			std::cerr << "(ERROR) " << _device << ": VIDIOC_S_PARM ("
			          << std::to_string(_IOC_NR(VIDIOC_S_PARM))
			          << "): " << std::strerror(errno) << std::endl;
			throw std::runtime_error(_device +
			                         ": VIDIOC_S_PARM fails: " + std::strerror(errno));
		}
	}

	if (running) // restart stream if needed
		start();
}

void
CameraV4L2::_updateRingBufferSize() {
	// RingBuffer size is setup during start()
	if (isRunning()) {
		stop();
		start();
	}
}

extern "C" {
//! @brief small helper to remove warning (old style cast)
static const void* mapFailed = MAP_FAILED;
}

void
CameraV4L2::initMmapStreaming() {
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": allocating memory (" << _ringBufferSize
		          << " buffers)" << std::endl;
	}
	// First request a number of buffer.
	struct v4l2_requestbuffers req;
	std::memset(&req, 0, sizeof(req));
	req.count  = _ringBufferSize;
	req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;
	if (-1 == ::ioctl(_fd, VIDIOC_REQBUFS, &req)) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_REQBUFS ("
		          << std::to_string(_IOC_NR(VIDIOC_REQBUFS))
		          << "): " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device +
		                         ": VIDIOC_REQBUFS fails: " + std::strerror(errno));
	}
	if (req.count != _ringBufferSize) {
		throw std::runtime_error(_device +
		                         ": Insufficient memory to create "
		                         "enough RAW buffer in the kernel "
		                         "ring buffer.");
	}

	// Get pointer and size of each one
	for (std::uint32_t i = 0; i < _ringBufferSize; ++i) {
		struct v4l2_buffer buf;
		std::memset(&buf, 0, sizeof(buf));
		buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index  = i;
		if (-1 == ::ioctl(_fd, VIDIOC_QUERYBUF, &buf)) {
			std::cerr << "(ERROR) " << _device << ": VIDIOC_QUERYBUF ("
			          << std::to_string(_IOC_NR(VIDIOC_QUERYBUF))
			          << "): " << std::strerror(errno) << std::endl;
			throw std::runtime_error(_device + ": VIDIOC_QUERYBUF fails: " +
			                         std::to_string(i) + ": " + std::strerror(errno));
		}

		// MMAP them
		void* startAddr =
		  ::mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, _fd, buf.m.offset);
		if (mapFailed == startAddr) {
			throw std::runtime_error(_device + ": Cannot mmap kernel buffer " +
			                         std::to_string(i) + ": " + strerror(errno));
		}
		_v4l2Buffers.push_back(std::make_pair(startAddr, buf.length));

		// Enqueue them
		if (-1 == ::ioctl(_fd, VIDIOC_QBUF, &buf)) {
			throw std::runtime_error(_device + ": VIDIOC_QBUF fails: " + std::to_string(i) +
			                         ": " + strerror(errno));
		}
	} // end of For
}

void
CameraV4L2::initMmapStreaming(int bufferCount) {
	_ringBufferSize = bufferCount;
	initMmapStreaming();
}

void
CameraV4L2::unInitMmapStreaming() {
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": deallocating memory" << std::endl;
	}
	bool success = true;
	for (auto& i : _v4l2Buffers) {
		if (::munmap(i.first, i.second)) {
			success = false;
		}
	}
	_v4l2Buffers.clear();
	if (!success) {
		throw std::runtime_error(_device +
		                         ": Error when unmapping RAW buffer: " + strerror(errno));
	}

	// Remove all buffer if any i.e. request 0 buffer
	struct v4l2_requestbuffers req;
	std::memset(&req, 0, sizeof(req));
	req.count  = 0;
	req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;
	if (-1 == ::ioctl(_fd, VIDIOC_REQBUFS, &req)) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_REQBUFS ("
		          << std::to_string(_IOC_NR(VIDIOC_REQBUFS))
		          << "): " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device +
		                         ": VIDIOC_REQBUFS fails: " + std::strerror(errno));
	}
	if (req.count != 0) {
		throw std::runtime_error(_device +
		                         ": Can't remove all previously allocated buffer.");
	}
}

void
CameraV4L2::startStreaming() {
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": start streaming" << std::endl;
	}
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == ::ioctl(_fd, VIDIOC_STREAMON, &type)) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_STREAMON ("
		          << std::to_string(_IOC_NR(VIDIOC_STREAMON))
		          << "): " << std::strerror(errno) << std::endl;
		unInitMmapStreaming();
		throw std::runtime_error(_device +
		                         ": Cannot start streaming: " + std::strerror(errno));
	}
}

void
CameraV4L2::stopStreaming() {
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": stop streaming" << std::endl;
	}
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == ::ioctl(_fd, VIDIOC_STREAMOFF, &type)) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_STREAMOFF ("
		          << std::to_string(_IOC_NR(VIDIOC_STREAMOFF))
		          << "): " << std::strerror(errno) << std::endl;
		unInitMmapStreaming();
		throw std::runtime_error(_device +
		                         ": Error stopping streaming: " + std::strerror(errno));
	}
}

v4l2_buffer
CameraV4L2::lockKernelBuffer() {
	v4l2_buffer buffer;
	std::memset(&buffer, 0, sizeof(struct v4l2_buffer));
	buffer.index  = 0;
	buffer.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buffer.memory = V4L2_MEMORY_MMAP;
	//! 1. First try to DQBUF a buffer if any.
	int result = ::ioctl(_fd, VIDIOC_DQBUF, &buffer);
	if (-1 == result && EAGAIN == errno) {
		//! 2. Otherwise wait for a new frame.
		if (_enableVerbose) {
			std::cout << "(VERBOSE) " << _device << ": no buffer available wait for a new one"
			          << std::endl;
		}
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(_fd, &fds);
		struct timeval timeout;
		timeout.tv_sec  = 1;
		timeout.tv_usec = 0;
		int r           = ::select(_fd + 1, &fds, NULL, NULL, &timeout);
		if (0 == r) {
			std::cerr << "(ERROR) " << _device << ": Timeout (1s) !" << std::endl;
			throw std::runtime_error(
			  _device + ": Can't get new image from driver: timeout (1s) on select");
		} else if (-1 == r) {
			std::cerr << "(ERROR) " << _device
			          << ": Can't get new image from driver: " << std::strerror(errno)
			          << std::endl;
			throw std::runtime_error(
			  _device + ": Can't get new image from driver: " + std::strerror(errno));
		}
		result = ::ioctl(_fd, VIDIOC_DQBUF, &buffer);
	}
	if (0 != result) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_DQBUF ("
		          << std::to_string(_IOC_NR(VIDIOC_DQBUF)) << "): " << std::strerror(errno)
		          << std::endl;
		throw std::runtime_error(
		  _device +
		  ": Cannot dequeue v4l2 buffer from kernel ring buffer: " + strerror(errno));
	}

	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": lock buffer,"
		          << " timecode: " << std::setw(3) << int32_t(buffer.sequence) << "seq"
		          << " timestamp: " << std::setw(4) << buffer.timestamp.tv_sec << "s, "
		          << std::setw(3) << buffer.timestamp.tv_usec / 1000 << "ms" << std::endl;
	}
	return buffer;
}

void
CameraV4L2::releaseKernelBuffer(v4l2_buffer& buffer) {
	if (_enableVerbose) {
		std::cout << "(VERBOSE) " << _device << ": release buffer,"
		          << " timecode: " << std::setw(3) << int32_t(buffer.sequence) << "seq"
		          << std::endl;
	}
	if (-1 == ::ioctl(_fd, VIDIOC_QBUF, &buffer)) {
		std::cerr << "(ERROR) " << _device << ": VIDIOC_QBUF ("
		          << std::to_string(_IOC_NR(VIDIOC_QBUF)) << "): " << std::strerror(errno)
		          << std::endl;
		throw std::runtime_error(_device +
		                         ": Cannot release v4l2 buffer: " + strerror(errno));
	}
}

void
CameraV4L2::printBuffer(v4l2_buffer& buffer) const {
	if (!::isatty(::fileno(::stdout))) {
		void* data       = _v4l2Buffers[buffer.index].first;
		size_t size      = _v4l2Buffers[buffer.index].second;
		ssize_t size_out = ::fwrite(data, size, 1, ::stdout);
		if (size_out != 1) {
			throw std::runtime_error(std::strerror(errno));
		}
	}
}
